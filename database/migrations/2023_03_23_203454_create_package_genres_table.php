<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageGenresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_genres', function (Blueprint $table) {
            $table->unsignedInteger('package_id');
            $table->foreign('package_id')
                ->references('id')
                ->on('packages')->onDelete('cascade');
            $table->unsignedInteger('genre_id');
            $table->foreign('genre_id')
                ->references('id')
                ->on('genres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_genres');
    }
}
