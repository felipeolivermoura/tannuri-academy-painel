<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    use HasFactory;

    public function wishlist()
    {
        return $this->belongsToMany(
            'App\Wishlist',
            'playlists_wishlists',
            'playlist_id',
            'wishlist_id'
        );
    }
}
