<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class SubCategoryController extends Controller
{


  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

  public function __construct()
  {
    $this->middleware('permission:label.view', ['only' => ['index']]);
    $this->middleware('permission:label.create', ['only' => ['create', 'store']]);
    $this->middleware('permission:label.edit', ['only' => ['edit', 'update']]);
    $this->middleware('permission:label.delete', ['only' => ['destroy', 'bulk_delete']]);
  }
  public function index(Request $request)
  {
    $categories = SubCategory::select('id', 'title', 'created_at', 'updated_at')->get();
    if ($request->ajax()) {
      return \Datatables::of($categories)

        ->addIndexColumn()
        ->addColumn('checkbox', function ($row) {
          $html = '<div class="inline">
                    <input type="checkbox" form="bulk_delete_form" class="filled-in material-checkbox-input" name="checked[]" value="' . $row->id . '" id="checkbox' . $row->id . '">
                    <label for="checkbox' . $row->id . '" class="material-checkbox"></label>
                  </div>';

          return $html;
        })

        ->addColumn('title', function ($row) {

          return $row->title;
        })
        ->addColumn('created_at', function ($row) {

          return date('F d, Y', strtotime($row->created_at));
        })
        ->addColumn('updated_at', function ($row) {

          return date('F d, Y', strtotime($row->updated_at));
        })

        ->addColumn('action', 'admin.subcategory.action')
        ->rawColumns(['checkbox', 'title', 'created_at', 'action', 'updated_at'])
        ->make(true);
    }
    return view('admin.subcategory.index', compact('categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $cats = Category::where("status", true)->get();
    $categories = collect();
    foreach ($cats as $category) {
      $categories->put($category->id, $category->title);
    };
    return view('admin.subcategory.create', compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if (env('DEMO_LOCK') == 1) {
      return back()->with('deleted', __('This action is disabled in the demo !'));
    }
    $request->validate(
      [
        'title' => 'required',
        'category_id' => 'required',
      ],
      [
        'category_id.required' => 'Select a category',
      ]
    );

    $input = $request->all();
    $query = new SubCategory();
    $query->title = $input['title'];
    $query->category_id = $input['category_id'];
    try {
      $query->save();
      return back()->with('success', __('Category created successfully !'));
    } catch (\Exception $e) {
      return back()->with('deleted', $e->getMessage())->withInput();
    }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Lable  $lable
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $sub_category = SubCategory::find($id);
    $cats = Category::where("status", true)->get();
    $categories = collect();
    foreach ($cats as $category) {
      $categories->put($category->id, $category->title);
    };
    return view('admin.subcategory.edit', compact('sub_category', 'categories'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\SubCategory  $category
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if (env('DEMO_LOCK') == 1) {
      return back()->with('deleted', __('This action is disabled in the demo !'));
    }
    $query = SubCategory::find($id);
    $request->validate(
      [
        'title' => 'required',
        'category_id' => 'required'
      ],
      [
        'category_id.required' => 'Selecet a category'
      ]
    );

    $input = $request->all();

    $query->title = $input['title'];
    $query->category_id = $input['category_id'];
    try {
      $query->update();
      return redirect('admin/sub_category')->with('success', __('Sub Category updated Successfully!'));
    } catch (\Exception $e) {
      return back()->with('deleted', $e->getMessage())->withInput();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Lable  $lable
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    if (env('DEMO_LOCK') == 1) {
      return back()->with('deleted', __('This action is disabled in the demo !'));
    }
    $query = SubCategory::find($id);
    if (isset($query) && $query != null) {
      $query->delete();
      return back()->with('deleted', __('Sub Category has been deleted !'));
    } else {
      return back()->with('deleted', "Not found");
    }
  }

  public function bulk_delete(Request $request)
  {
    if (env('DEMO_LOCK') == 1) {
      return back()->with('deleted', __('This action is disabled in the demo !'));
    }
    $validator = Validator::make($request->all(), ['checked' => 'required']);

    if ($validator->fails()) {

      return back()
        ->with('deleted', __('Please select one of them to delete'));
    }

    foreach ($request->checked as $checked) {

      $category = SubCategory::findOrFail($checked);

      $category->delete();
    }

    return back()->with('deleted', __('Sub Category has been deleted'));
  }
}
