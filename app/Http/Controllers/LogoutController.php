<?php

namespace App\Http\Controllers;

use App\PaypalSubscription;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class LogoutController extends Controller
{
  
    public function logout()
    {

        if (Auth::user()['is_admin'] == 1) {
            //In case user is admin
            Auth::logout();
            Session::flush();
            return redirect('/')->with('success', 'Logged out !');
        } elseif (isset(Auth::user()->subscriptions)) {

            if (isset($mlt_screen) && $mlt_screen == 1) {

                $activesubsription = PaypalSubscription::where('user_id', Auth::user()->id)->where('status', '=', 1)->orderBy('created_at', 'desc')->first();

                if (isset($activesubsription)) {
                    //In case screen not found
                    Auth::logout();
                    Session::flush();
                    return redirect('/')->with('success',__('Logged out !'));

                } else {
                    //In case user is not subscribed
                    Auth::logout();
                    Session::flush();
                    return redirect('/')->with('success', __('Logged out !'));
                }
            } else {
                //In case user is not subscribed
                Auth::logout();
                Session::flush();
                return redirect('/')->with('success', __('Logged out !'));
            }

        } else {
            //In case user is not subscribed
            Auth::logout();
            Session::flush();
            return redirect('/')->with('success', __('Logged out Successfully !'));
        }

    }
}
