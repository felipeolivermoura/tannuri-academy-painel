<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use HasFactory;

    protected $fillable = [
    	'title',
    	'category_id',
    ];

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function movies()
    {
        return $this->belongsToMany(
            'App\Movie',
            'categories_movies',
            'sub_category_id',
            'movie_id'
        );
    }
}
