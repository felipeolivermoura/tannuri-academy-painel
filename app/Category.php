<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Category extends Model
{
    use HasFactory;
    use HasTranslations;

    public $translatable = ['title'];

    public function toArray()
    {
        $attributes = parent::toArray();

        foreach ($this->getTranslatableAttributes() as $name) {
            $attributes[$name] = $this->getTranslation($name, app()->getLocale());
        }

        return $attributes;
    }

    protected $fillable = ['title'];

    public function movies()
    {
        return $this->belongsToMany(
            'App\Movie',
            'categories_movies',
            'category_id',
            'movie_id'
        );
    }

    public function subs()
    {
        return $this->hasMany('App\SubCategory');
    }
}
