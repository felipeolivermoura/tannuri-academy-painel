<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = [
        'user_id',
        'movie_id',
        'season_id',
        'added',
    ];

    public function users()
    {
        return $this->belongsTo('App\User')->withDefault();
    }
}
