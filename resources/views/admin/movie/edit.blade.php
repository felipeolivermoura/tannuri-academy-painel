@extends('layouts.master')
@section('title',__('Edit')." "." - $movie->title")
@section('breadcum')
	<div class="breadcrumbbar">
                <h4 class="page-title">{{ __('HOME') }}</h4>
                <div class="breadcrumb-list">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{url('/admin')}}">{{ __('Dashboard') }}</a></li>
                      <li class="breadcrumb-item active" aria-current="page">{{ __('Editar Movie') }}</li>
                    </ol>
                </div> 
    </div>
@endsection
@section('maincontent')
<div class="contentbar">
  <div class="row">
    @if ($errors->any())  
    <div class="alert alert-danger" role="alert">
      @foreach($errors->all() as $error)     
      <p>{{ $error}}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true" style="color:red;">&times;</span></button>
      </p>
      @endforeach  
    </div>
    @endif
    {{-- vimeo API Modal Start --}}
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleLargeModalLabel">{{__("Search From Vimeo API")}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @if(is_null(env('VIMEO_ACCESS')))
            <p>{{__('Make Sure You Have Added Vimeo API KeyIn')}} <a href="{{url('admin/api-settings')}}">{{__('API Settings')}}</a></p>
          @endif

          <div class="modal-body">
            <div class="box box-danger">
              <div class="box-header">
                <div class="box-title">{{__('Instructions')}}</div>
              </div>
              <form action="" method="post" name="vimeo-yt-search" id="vimeo-yt-search">
                <input type="search" name="vimeo-search" id="vimeo-search" placeholder="{{__('Search')}}..." class="ui-autocomplete-input" autocomplete="off">
                <button class="icon" id="vimeo-searchBtn"></button>
              </form>
              <input type="hidden" id="vpageToken" value="">
              <div class="btn-group" role="group" aria-label="...">
                <button type="button" id="vpageTokenPrev" value="" class="btn btn-default">{{__('Prev')}}</button>
                <button type="button" id="vpageTokenNext" value="" class="btn btn-default">{{__('Next')}}</button>
              </div>
            </div>
            <div id="vimeo-watch-content" class="vimeo-watch-main-col vimeo-card vimeo-card-has-padding">
                <ul id="vimeo-watch-related" class="vimeo-video-list">
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{----vimeo API ModalEnd --}}

    {{-- youtube API Modal Start --}}
    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleLargeModalLabel">{{__("Search From Youtube API")}}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              @if(is_null(env('YOUTUBE_API_KEY')))
            <p>{{__('Make Sure You Have Added Youtube APIKey In')}} <a href="{{url('admin/api-settings')}}">{{__('API Settings')}}</a></p>
            @endif

              <div class="modal-body">
                  <div class="box box-danger">
                      <div class="box-header">
                        <div class="box-title">{{__('Instructions')}}</div>
                      </div>
                      <form action="" method="post" name="hyv-yt-search" id="hyv-yt-search">
                        <input type="search" name="hyv-search" id="hyv-search" placeholder="{{__('Search')}}..." class="ui-autocomplete-input" autocomplete="off">
                        <button class="icon" id="hyv-searchBtn"></button>
                    </form>
                    <input type="hidden" id="vpageToken" value="">
                    <div>
                      <input type="hidden" id="pageToken" value="">
                      <div class="btn-group" role="group" aria-label="...">
                        <button type="button" id="pageTokenPrev" value="" class="btn btn-default">{{__('Prev')}}</button>
                        <button type="button" id="pageTokenNext" value="" class="btn btn-default">{{__('Next')}}</button>
                      </div>
                  </div>
                  <div id="hyv-watch-content" class="hyv-watch-main-col hyv-card hyv-card-has-padding">
                      <ul id="hyv-watch-related" class="hyv-video-list">
                      </ul>
                  </div>
                    </div>
              </div>
          </div>
      </div>
    </div>
    {{----youtube API ModalEnd --}}

    <!-- <div class="col-lg-7 col-xl-8"> -->
    <div class="col-lg-12">
      <div class="card m-b-50 movie-create-card">
        <div class="card-header">
          <a href="{{url('admin/movies')}}" class="float-right btn btn-primary-rgba mr-2"><i
            class="feather icon-arrow-left mr-2"></i>{{ __('Voltar') }}</a>
          <h5 class="box-title">{{__('Editar Movie')}}</h5>
        </div>
        <div class="card-body ml-2">
          {!! Form::model($movie, ['method' => 'PATCH', 'action' => ['MovieController@update',$movie->id], 'files' => true]) !!}
          <div class="bg-info-rgba p-4 mb-4 rounded">
            <div class="row">
              <div class="col-lg-4 col-md-4">
                <label class="text-dark"for="">{{__('Permitir pesquisar este video por título')}}:</label>
                <br>
                <label class="switch">
                  <input type="checkbox" name="movie_by_id" {{ $movie->fetch_by == "title" ? "checked" : "" }} class="custom_toggle" id="movi_id">
                  <span class="slider round"></span>
                </label>
              </div>
              <div class="col-lg-4 col-md-4">
                <div id="movie_title" class="form-group text-dark{{ $errors->has('title') ? ' has-error' : '' }}">
                  {!! Form::label('title', __('Titulo do video')) !!}
                  {!! Form::text('title', $movie->title, ['class' => 'form-control', 'placeholder' => __('Digite o titulo'), $movie->fetch_by == 'byID' ? 'readonly' : '']) !!}
                  <small class="text-danger">{{ $errors->first('title') }}</small>
                </div>
                <div id="movie_id" style="display: none;" class="form-group text-dark{{ $errors->has('title2') ? ' has-error' : '' }}">
                  {!! Form::label('title',__('ID do video')) !!}
                  {!! Form::text('title2', $movie->title2, ['class' => 'form-control', 'placeholder' =>__('Digite o ID do video'), $movie->fetch_by == 'byID' ? 'readonly' : '']) !!}
                  <small class="text-danger">{{ $errors->first('title2') }}</small>
                </div>
              </div>
              <div class="col-lg-4 col-md-4">
                <div id="movie_slug" class="form-group text-dark{{ $errors->has('slug') ? ' has-error' : '' }}">
                  {!! Form::label('slug', __('Slug do video')) !!}
                  {!! Form::text('slug', $movie->slug, ['class' => 'form-control', 'placeholder' =>__('Digite o slug')]) !!}
                  <small class="text-danger">{{ $errors->first('slug') }}</small>
                </div>
              </div>
            </div>
          </div>
          <div class="bg-info-rgba p-4 mb-4 rounded">
            <div class="row">
              <div class="col-md-12">
                {{-- select to upload code start from here --}}
                <div class="form-group text-dark{{ $errors->has('selecturl') ? ' has-error' : '' }} video_url" style="{{$movie['is_upcoming'] !=  1 ? ' ' : "display: none" }}">
                  {!! Form::label('selecturls', __('Add Video')) !!}
                  <select class="form-control select2" id="selecturl" name="selecturl">
                    <option value="vimeoapi">{{__('Vimeo Api')}}</option>
                    @if(isset($video_link['upload_video']) && $video_link['upload_video'] != '')
                      <option value="uploadvideo" selected="">{{__('Upload Video')}}</option>
                      @else
                        <option value="uploadvideo">{{__('Upload Video')}}</option>
                    @endif
                  </select>
                  <small class="text-danger">{{ $errors->first('selecturl') }}</small>
                </div>

                {{-- youtube and vimeo url --}}
                <div id="ready_url" style="{{isset($video_link['ready_url']) && $video_link['ready_url'] != '' ? '' : "display: none" }}" class="form-group text-dark{{ $errors->has('ready_url') ? ' has-error' : '' }}">
                  <label id="ready_url_text">{{__('')}}</label>
                  <p class="inline info"> {{__('Insira o link do video')}}</p> <button type="button" onclick="encript()" id="encryptlink" class="btn-primary-rgba">{{__('Criptografar Link')}}</button>
                  {!! Form::text('ready_url',isset($video_link['ready_url']) && $video_link['ready_url'] ? $video_link['ready_url'] : NULL , ['class' => 'form-control','id'=>'apiUrl']) !!}
                  <small class="text-danger">{{ $errors->first('ready_url') }}</small>
                </div>
                {{-- upload video --}}
                <div id="uploadvideo" style="{{isset($video_link['upload_video']) && $video_link['upload_video']!='' ? '' : "display: none" }}" class="form-group text-dark{{ $errors->has('upload_video') ? ' has-error' : '' }} input-file-block">
                  <label>Nome do arquivo: <span>{{isset($video_link['upload_video']) && $video_link['upload_video'] != NULL ? $video_link['upload_video'] : '' }}</span></label>
                  <br>
                    {!! Form::label('upload_video', 'Upload Video') !!} - <p class="inline info">Escolher Video</p>
                  
                  <div class="input-group">
                  
                    <input type="text" class="form-control" id="upload_video" name="upload_video" >
                    <span class="input-group-addon midia-toggle-upload_video" data-input="upload_video">{{__('Escolher Video')}}</span>
                    
                  </div>
                  <small class="text-danger">{{ $errors->first('upload_video') }}</small>
                </div>
                {{-- select to upload or add links code ends here --}}
              </div>
            </div>
          </div>  
          <div class="bg-info-rgba p-4 mb-4 rounded">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group text-dark{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    {!! Form::label('category_id', __('Categorias')) !!}
                    {!! Form::select('category_id[]', $categories, $movie_cats, ['class' => 'form-control select2', 'multiple']) !!}
                    <small class="text-danger">{{ $errors->first('category_id') }}</small>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group text-dark{{ $errors->has('sub_category_id') ? ' has-error' : '' }}">
                    {!! Form::label('sub_category_id', __('Sub Categorias')) !!}
                    {!! Form::select('sub_category_id[]', $sub_categories, $movie_sub_cats, ['class' => 'form-control select2', 'multiple']) !!}
                    <small class="text-danger">{{ $errors->first('sub_category_id') }}</small>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group text-dark">
                  <label for="">{{__('Palavra chave')}}: </label>
                  <input name="keyword" type="text" class="form-control" value="{{ $movie->keyword }}" />
                </div>
              </div>
              <div class="col-lg-12 col-md-12">
                <div class="form-group text-dark">
                  <label for="">{{__('Descrição do video')}}: </label>
                  <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ $movie->description }}</textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="bg-info-rgba p-4 mb-4 rounded">
            <div class="row">
              <div class="col-lg-8 col-md-8">
                <div class="form-group text-dark">
                  {!! Form::label('', __('Escolha capa e tumbmail do video')) !!}
                  <br>
                  <label class="switch for-custom-image">
                    {!! Form::checkbox('', 1, 0, ['class' => 'custom_toggle']) !!}
                    <span class="slider round"></span>
                  </label>
                </div>
                <div class="upload-image-main-block" id="custom_thumb_hide" >
                    <div class="row">
                      <div class="col-lg-4 col-md-5 mb-4">
                        <label>{{__('Thumbnail')}}</label>
                        <div class="thumbnail-img-block">
                          @if(isset($movie->thumbnail) && $movie->thumbnail != NULL)
                          <img src="{{url('/images/movies/thumbnails/'.$movie->thumbnail)}}" id="thumbnail" class="img-fluid" alt="">
                        @else
                        <img src="{{ url('images/default-thumbnail.jpg')}}" id="thumbnail" class="img-fluid" alt="">
                        @endif
                        </div>
                        <div class="input-group">
                          <input id="img_upload_input" type="file" name="thumbnail" class="form-control" onchange="readURL(this);" />
                        </div>
                      </div>
                      <div class="col-lg-8 col-md-7 mb-4">
                        <label>{{__('Posters')}}</label>
                        <div class="poster-img-block">
                          @if(isset($movie->poster) && $movie->poster != NULL)
                            <img src="{{url('/images/movies/posters/'.$movie->poster)}}" id="poster" class="img-fluid" alt="">
                          @else
                          <img src="{{ url('images/default-poster.jpg')}}" id="poster" class="img-fluid" alt="">
                          @endif
                        </div>
                        <div class="input-group">
                          <input id="img_upload_input_one" type="file" name="poster" class="form-control" onchange="readURL(this);" />
                        </div>
                      </div>
                    </div>
                </div>
              </div>
              <div class="col-lg-12 col-md-12">
                <div class="menu-block menu-block-input" id="kids_mode_hide">
                  <h6 class="menu-block-heading mb-3">{{__('Please Select Menu')}}</h6>
                  @if (isset($menus) && count($menus) > 0)
                  <div class="row">
                    @foreach ($menus as $menu)
                    <div class="col-lg-3 col-md-6">
                      <div class="row">
                        <div class="col-lg-4 col-md-8 col-6">
                          {{$menu->name}}
                        </div>
                        @php
                        $checked = null;
                        if (isset($menu->menu_data) && count($menu->menu_data) > 0) {
                          if ($menu->menu_data->where('movie_id', $movie->id)->where('menu_id', $menu->id)->first() != null) {
                            $checked = 1;
                          }
                        }
                        @endphp
                        <div class="col-lg-8 col-md-4 col-6 pad-0">
                          <div class="inline">
                            @if ($checked == 1)
                            <input type="checkbox" class="filled-in material-checkbox-input" name="menu[]" value="{{$menu->id}}" id="checkbox{{$menu->id}}" checked>
                            <label for="checkbox{{$menu->id}}" class="material-checkbox"></label>
                            @else
                            <input type="checkbox" class="filled-in material-checkbox-input" name="menu[]" value="{{$menu->id}}" id="checkbox{{$menu->id}}">
                            <label for="checkbox{{$menu->id}}" class="material-checkbox"></label>
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="bg-info-rgba p-4 mb-4 rounded">
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="switch-field">
                  <div class="switch-title text-dark">{{__('Mais configurações?')}}?</div>
                  <input type="radio" id="switch_left" class="imdb_btn d-none" name="tmdb" value="Y" {{$movie->tmdb == 'Y' ? 'checked' : ''}}/>
                  <label for="switch_left" class="d-none">{{__('TMDB')}}</label>
                  <input type="radio" id="switch_right" class="custom_btn" name="tmdb" value="N" {{$movie->tmdb != 'Y' ? 'checked' : ''}}/>
                  <label for="switch_right">{{__('Configurações')}}</label>
                </div>
                <div id="custom_dtl" class="custom-dtl">
                  <div class="row">
                    <div class="col-lg-4 col-md-6">
                      <div class="form-group text-dark{{ $errors->has('genre_id') ? ' has-error' : '' }}">
                        {!! Form::label('genre_id', __('Tipo de faixa')) !!}
                        <div class="input-group">
                          <select name="genre_id[]" id="genre_id" class="form-control select2" multiple="multiple">
                            @if(isset($old_genre) && count($old_genre) > 0)
                            @foreach($old_genre as $old)
                            <option value="{{$old->id}}" selected="selected">{{$old->name}}</option> 
                            @endforeach
                            @endif
                            @if(isset($genre_ls))
                            @foreach($genre_ls as $rest)
                            <option value="{{$rest->id}}">{{$rest->name}}</option> 
                            @endforeach
                            @endif
                          </select>
                        </div>
                        <small class="text-danger">{{ $errors->first('genre_id') }}</small>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                      <div class="form-group text-dark{{ $errors->has('duration') ? ' has-error' : '' }}">
                        {!! Form::label('duration', __('Duração')) !!}
                        {!! Form::text('duration', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('duration') }}</small>
                      </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                      <div class="form-group text-dark{{ $errors->has('rating') ? ' has-error' : '' }}">
                        {!! Form::label('rating', __('Estrelas')) !!}
                        {!! Form::text('rating', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('rating') }}</small>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group text-dark{{ $errors->has('detail') ? ' has-error' : '' }}">
                        {!! Form::label('detail',__('Description')) !!}
                        {!! Form::textarea('detail', null, ['class' => 'form-control materializes-textarea', 'rows' => '5']) !!}
                        <small class="text-danger">{{ $errors->first('detail') }}</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group ">
                <button type="reset" class="btn btn-success-rgba">{{__('Redefinir')}}</button>
                <button type="submit" class="btn btn-primary-rgba"><i class="fa fa-check-circle"></i>
                  {{ __('Atualizar') }}</button>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
          <div class="clear-both"></div>
        </div>
      </div>
    </div>
  </div>
</div>

    <!-- Add Subtitle Modal -->
<div id="submodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{__('Add Subtitle')}}</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="{{ route('add.subtitle',$movie->id) }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
          <table class="table table-bordered" id="dynamic_field">  
            <tr> 
              <td>
                <div class="form-group{{ $errors->has('sub_t') ? ' has-error' : '' }} input-file-block">
                  <input type="file" name="sub_t[]"/>
                  <p class="info">{{__('Choose Subtitle File')}} ex. subtitle.srt, or. txt</p>
                  <small class="text-danger">{{ $errors->first('sub_t') }}</small>
                </div>
              </td>

              <td>
                <input type="text" name="sub_lang[]" placeholder="Subtitle Language" class="form-control name_list" />
              </td>  
              <td><button type="button" name="add" id="add" class="btn btn-xs btn-success">
                <i class="fa fa-plus"></i>
                </button>
              </td>  
            </tr>  
          </table>
        </div>
        <div class="modal-footer">
          <div class="btn-group pull-right">
            <button type="reset" class="btn btn-success-rgba">{{__('Reset')}}</button>
            <button type="submit" class="btn btn-primary-rgba">{{__('Create')}}</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection 
@section('script')
{{-- <script>
  function readURL(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#' + input.name).attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
</script> --}}
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> -->

<!------------------------- ajax directors ------------------>
<script type="text/javascript">
  $(document).ready(function(){

     $(".directorList").select2({
      ajax: { 
       url: "{{ route('listofd') }}",
       type: "GET",
       dataType: 'json',
       delay: 250,
       data: function (params) {
        return {
          searchTerm: params.term // search term
        };
       },
       processResults: function (response) {
         return {
            results: response
         };
       },
       cache: true
      }
     });
});
</script>
<!-------------- end ajax director---------------->


<!------------------------- ajax actor ------------------>
<script type="text/javascript">
  $(document).ready(function(){

     $(".actorList").select2({
      ajax: { 
       url: "{{ route('listofactor') }}",
       type: "GET",
       dataType: 'json',
       delay: 250,
       data: function (params) {
        return {
          searchTerm: params.term // search term
        };
       },
       processResults: function (response) {
         return {
            results: response
         };
       },
       cache: true
      }
     });
});

$(document).ready(function(){
$('#send_form_actor').click(function(e){
   e.preventDefault();
   /*Ajax Request Header setup*/
   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

   $('#send_form_actor').html('Creating..');
   
   /* Submit form data using ajax*/
   $.ajax({
      url: "{{ route('ajax.actor')}}",
      method: 'GET',
      data: $('#editactor').serialize(),
      datatype : 'html',
      success: function(response){
        
         //------------------------
            $('#send_form_actor').html('Create');
            $('#msg_div').show();
            $('#res_message').html(response.msg);

            document.getElementById("editactor").reset(); 
            setTimeout(function(){
            $('#res_message').hide();
            $('#msg_div').hide();
            $('#AddActorModal').modal('hide');

            },1000);
         //--------------------------
      }});
   });
});
</script>
<!-------------- end ajax actor---------------->

<script>
  $(document).ready(function(){
    // $("#selecturl").select2({
    //   placeholder: "Add Video Through...",

    // });
    $('#selecturl').change(function(){  
     selecturl = document.getElementById("selecturl").value;
   if (selecturl == 'iframeurl') {
    $('#ifbox').show();
    $('#subtitle_section').hide();
    $('#ready_url').hide();
    $('#custom_url').hide();
    $('#uploadvideo').hide();

  }else if(selecturl=='customurl'){
       $('#ifbox').hide();
       $('#ready_url').show();
       $('#subtitle_section').show();
       $('#ready_url_text').text('Enter Custom URL or Vimeo or Youtube URL');
       $('#custom_url').hide();
       $('#uploadvideo').hide();
   }
   else if (selecturl == 'uploadvideo') {
     $('#uploadvideo').show();
     $('#subtitle_section').show();
     $('#ready_url').hide();
     $('#ifbox').hide();
     $('#custom_url').hide();

   }
    else if (selecturl == 'youtubeapi') {
   $('#ready_url').show();
   $('#subtitle_section').show();
   $('#custom_url').hide();
   $('#ifbox').hide();
   $('#ready_url_text').text('Import From Youtube API');
   $('#uploadvideo').hide();

 }else if(selecturl=='vimeoapi'){
   $('#ifbox').hide();
   $('#ready_url').show();
   $('#subtitle_section').show();
   $('#custom_url').hide();
   $('#ready_url_text').text('Import From Vimeo API');
   $('#uploadvideo').hide();
 }
 else if(selecturl=='multiqcustom'){
   $('#ifbox').hide();
   $('#ready_url').hide();
   $('#subtitle_section').show();
   $('#custom_url').show();
   $('#uploadvideo').hide();
 }



});
    var i= 1;
    $('#add').click(function(){  
     i++;  
     $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td><input type="file" name="sub_t[]"/></td><td><input type="text" name="sub_lang[]" placeholder="Subtitle Language" class="form-control name_list" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn-sm btn_remove">X</button></td></tr>');  
   });

    $(document).on('click', '.btn_remove', function(){  
     var button_id = $(this).attr("id");   
     $('#row'+button_id+'').remove();  
   });  

   

    $('.upload-image-main-block').hide();
    @if($movie->tmdb == 'N')
    $('#custom_dtl').show();
    @endif
    @if ($movie->subtitle == 0)
    $('.subtitle_list').hide();
    $('#subtitle-file').hide();
    @endif 
    @if($movie->series == 0)
    $('.movie_id').hide();
    @endif
    $('input[name="subtitle"]').click(function(){
     if($(this).prop("checked") == true){
       $('.subtitle_list').fadeIn();
       $('#subtitle-file').fadeIn();
     }
     else if($(this).prop("checked") == false){
      $('.subtitle_list').fadeOut();
      $('#subtitle-file').fadeOut();
    }
  });
    $('.for-custom-image input').click(function(){
      if($(this).prop("checked") == true){
        $('.upload-image-main-block').fadeIn();
      }
      else if($(this).prop("checked") == false){
        $('.upload-image-main-block').fadeOut();
      }
    });
    $('input[name="series"]').click(function(){
     if($(this).prop("checked") == true){
       $('.movie_id').fadeIn();
     }
     else if($(this).prop("checked") == false){
      $('.movie_id').fadeOut();
    }
  });

  $('input[name="is_protect"]').click(function(){
    if($(this).prop("checked") == true){
      $('.is_protect').fadeIn();
    }
    else if($(this).prop("checked") == false){
      $('.is_protect').fadeOut();
    }
  });

  

      $('#kids_mode').on('change',function(){
    if($('#kids_mode').is(':checked')){
      $('#kids_mode_show').show('fast');
        $('#kids_mode_hide').hide('fast');
      $('#is_kids').show('fast');
      $('#is_not_kids').hide('fast');
    }else{
      $('#kids_mode_hide').show('fast');
        $('#kids_mode_show').hide('fast');
      $('#is_not_kids').show('fast');
      $('#is_kids').hide('fast');
    }
      
    });

    $('#custom_thumb').on('change',function(){
    if($('#custom_thumb').is(':checked')){
      $('#custom_thumb_hide').show('fast');
        $('#custom_thumb_show').hide('fast');
    
    }else{
      $('#custom_thumb_show').show('fast');
        $('#custom_thumb_hide').hide('fast');
    }
      
    });


  $('input[name="is_upcoming"]').click(function(){
    if($(this).prop("checked") == true){
      $('.video_url').fadeOut();
      $('#ifbox').fadeOut();
      $('#uploadvideo').fadeOut();
      $('#custom_url').fadeOut();
      $('#ready_url').fadeOut();
      $('#upcoming_box').show();
    }
    else if($(this).prop("checked") == false){
      $('.video_url').fadeIn();
      $('#ifbox').show();
      $('#uploadvideo').hide();
      $('#custom_url').hide();
      $('#ready_url').hide();
      $('#upcoming_box').hide();
      }
  });

   $('input[name="is_custom_label"]').click(function(){
      if($(this).prop("checked") == true){
        $('#label_box').show();
      }
      else if($(this).prop("checked") == false){
        $('#label_box').hide();
      }
    });

  });
</script>
{{-- vimeo api code --}}

<script>
      (function($) {
          var videourl;
          vimeoApiCall();
          $("#vpageTokenNext").on( "click", function( event ) {
              $("#vpageToken").val($("#vpageTokenNext").val());
              vimeoApiCall();
          });
          $("#vpageTokenPrev").on( "click", function( event ) {
              $("#vpageToken").val($("#vpageTokenPrev").val());
              vimeoApiCall();
          });
          $("#vimeo-searchBtn").on( "click", function( event ) {
              vimeoApiCall();
              return false;
          });
          $( "#vimeo-search" ).autocomplete({
            source: function( request, response ) {
              //console.log(request.term);
              var sqValue = [];
              var accesstoken='{{env('VIMEO_ACCESS')}}';
              var myvimeourl='https://api.vimeo.com/videos?query=videos'+'&access_token=' + accesstoken +'&per_page=1';
              console.log(myvimeourl);
              jQuery.ajax({
                  type: "GET",
                  url: myvimeourl,
                  dataType: 'jsonp',
                  
                  success: function(data){
                      console.log(data[1]);
                      obj = data[1];
                      jQuery.each( obj, function( key, value ) {
                          sqValue.push(value[0]);
                      });
                      response( sqValue);
                  }
              });
            },
            select: function( event, ui ) {
              setTimeout( function () { 
                  vimeoApiCall();
              }, 300);
            }
          });  
      })(jQuery);
function vimeoApiCall(){
  console.log('yeah i am here');
    var accesstoken='{{env('VIMEO_ACCESS')}}';
    var text=$("#vimeo-search").val();
   var next=  $("#vpageTokenNext").val();
   console.log('jxhh'+next);
   var prev= $("#vpageTokenPrev").val();
    var myvimeourl=null;
   if (next != null && next !='') {
     myvimeourl='https://api.vimeo.com'+next;
   }else if (prev != null && prev !='') {
       myvimeourl='https://api.vimeo.com'+prev;
   }else{
       myvimeourl='https://api.vimeo.com/videos?query='+ text + '&access_token=' + accesstoken+'&per_page=5';
   }
  
   console.log('url'+myvimeourl);
    $.ajax({
        cache: false,
     
        dataType: 'json',
        type: 'GET',
       
        url: myvimeourl,

    })
    .done(function(data) {
      console.log(data);
    // alert('duhjf');
        if ( data.paging.previous === null) {$("#vpageTokenPrev").hide();}else{$("#vpageTokenPrev").show();}
        if ( data.paging.next === null) {$("#vpageTokenNext").hide();}else{$("#vpageTokenNext").show();}
        var items = data.data, videoList = "";
     
        $("#vpageTokenNext").val(data.paging.next);
        $("#vpageTokenPrev").val(data.paging.previous);
        console.log(items);
        $.each(items, function(index,e) {
             
             videourl=e.link;
               // console.log(videourl);
            videoList = videoList 
            + '<li class="hyv-video-list-item" ><div class="hyv-thumb-wrapper"><p class="hyv-thumb-link"><span class="hyv-simple-thumb-wrap"><img alt="'+e.name+'" src="'+e.pictures.sizes[3].link+'" height="90"></span></p></div><div class="hyv-content-wrapper"><p  class="hyv-content-link">'+e.name+'<span class="title">'+e.description.substr(0, 105)+'</span><span class="stat attribution">by <span>'+e.user.name+'</span></span></p><button class="bn btn-info btn-sm inline" onclick=setVideovimeoURl("'+videourl+'")>Add</button></div></li>';
              
          
        });

        $("#vimeo-watch-related").html(videoList);
       
    });

}
</script>
<script>
  $('#movi_id').on('change',function(){
    if ($('#movi_id').is(':checked')){

      $('#txt2').text("Movie Created By Title:");
      $('#mv_t').removeAttr('readonly','readonly');
      $('#mv_i').attr('readonly','readonly');

    }else{
     $('#mv_i').removeAttr('readonly','readonly');
     $('#mv_t').attr('readonly','readonly');
     $('#txt2').text("Movie Created By ID:");
   }
 });

 
</script>
<script>
  $('#movi_id').on('change',function(){
    if ($('#movi_id').is(':checked')){
      $('#movie_title').show('fast');
      $('#movie_id').hide('fast');
    }else{
     $('#movie_id').show('fast');
     $('#movie_title').hide('fast');
   }
 });

 
</script>

{{-- youtube API code --}}
<script>
        (function($) {
           var videourl;
            youtubeApiCall();
            $("#pageTokenNext").on( "click", function( event ) {
                $("#pageToken").val($("#pageTokenNext").val());
                youtubeApiCall();
            });
            $("#pageTokenPrev").on( "click", function( event ) {
                $("#pageToken").val($("#pageTokenPrev").val());
                youtubeApiCall();
            });
            $("#hyv-searchBtn").on( "click", function( event ) {
                youtubeApiCall();
                return false;
            });
            $( "#hyv-search" ).autocomplete({
              source: function( request, response ) {
                //console.log(request.term);
                var sqValue = [];
                jQuery.ajax({
                    type: "POST",
                    url: "http://suggestqueries.google.com/complete/search?hl=en&ds=yt&client=youtube&hjson=t&cp=1",
                    dataType: 'jsonp',
                    data: jQuery.extend({
                        q: request.term
                    }, {  }),
                    success: function(data){
                        console.log(data[1]);
                        obj = data[1];
                        jQuery.each( obj, function( key, value ) {
                            sqValue.push(value[0]);
                        });
                        response( sqValue);
                    }
                });
              },
              select: function( event, ui ) {
                setTimeout( function () { 
                    youtubeApiCall();
                }, 300);
              }
            });  
        })(jQuery);
function youtubeApiCall(){
    $.ajax({
        cache: false,
        data: $.extend({
            key: '{{env('YOUTUBE_API_KEY')}}',
            q: $('#hyv-search').val(),
            part: 'snippet'
        }, {maxResults:15,pageToken:$("#pageToken").val()}),
        dataType: 'json',
        type: 'GET',
        timeout: 5000,
        url: 'https://www.googleapis.com/youtube/v3/search'
    })
    .done(function(data) {
        if (typeof data.prevPageToken === "undefined") {$("#pageTokenPrev").hide();}else{$("#pageTokenPrev").show();}
        if (typeof data.nextPageToken === "undefined") {$("#pageTokenNext").hide();}else{$("#pageTokenNext").show();}
        var items = data.items, videoList = "";
        $("#pageTokenNext").val(data.nextPageToken);
        $("#pageTokenPrev").val(data.prevPageToken);
        // console.log(items);
        $.each(items, function(index,e) {
             
             videourl="https://www.youtube.com/watch?v="+e.id.videoId;
               console.log(videourl);
            videoList = videoList 
            + '<li class="hyv-video-list-item" ><div class="hyv-content-wrapper"><p  class="hyv-content-link" title="'+e.snippet.title+'"><span class="title">'+e.snippet.title+'</span><span class="stat attribution">by <span>'+e.snippet.channelTitle+'</span></span></p><button class="bn btn-info btn-sm inline" onclick=setVideoURl("'+videourl+'")>Add</button></div><div class="hyv-thumb-wrapper"><p class="hyv-thumb-link"><span class="hyv-simple-thumb-wrap"><img alt="'+e.snippet.title+'" src="'+e.snippet.thumbnails.default.url+'" height="90"></span></p></div></li>';
              
          
        });

        $("#hyv-watch-related").html(videoList);
       
    });
}
    </script>
<script type="text/javascript">
  function setVideoURl(videourls){
    console.log(videourls);
  $('#apiUrl').val(videourls); 
    $('#myyoutubeModal').modal("hide");
  }
</script>
<script type="text/javascript">
  function setVideovimeoURl(videourls){
    console.log(videourls);
  $('#apiUrl').val(videourls); 
    $('#myvimeoModal').modal("hide");
  }
</script>
<script type="text/javascript">
  $(document).ready(function(){ 
    $('#selecturl').change(function() {
     $('#apiUrl').val('');  
        var opval = $(this).val(); //Get value from select element
        if(opval=="youtubeapi"){ //Compare it and if true
            $('#myyoutubeModal').modal("show"); //Open Modal
        }
        if(opval=="vimeoapi"){ //Compare it and if true
            $('#myvimeoModal').modal("show"); //Open Modal
        }
    });
});
</script>
<script>
  $('.seriescheck').on('change',function(){
      if($(this).is(':checked')){
        $('.mseries').attr('required','required');
      }else{
        $('.mseries').removeAttr('required');
      }
  });


</script>
<script>
  $('#subtitle_check').on('change',function(){

    if($('#subtitle_check').is(':checked')){
      $('.subtitle-box').show('fast');
    }else{
       $('.subtitle-box').hide('fast');
    }

  });
</script>
<script type="text/javascript">
    $(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
</script>
<script>
  $(".midia-toggle-upload_video").midia({
		base_url: '{{url('')}}',
    dropzone : {
          acceptedFiles: '.mp4,.m3u8'
        },
    directory_name: 'movies_upload'
	});

  $(".midia-toggle-upload_video_360").midia({
		base_url: '{{url('')}}',
    dropzone : {
          acceptedFiles: '.mp4,.m3u8'
        },
    directory_name: 'movies_upload/url_360'
	});

  $(".midia-toggle-upload_video_480").midia({
		base_url: '{{url('')}}',
    dropzone : {
          acceptedFiles: '.mp4,.m3u8'
        },
    directory_name: 'movies_upload/url_480'
	});

  $(".midia-toggle-upload_video_720").midia({
		base_url: '{{url('')}}',
    dropzone : {
          acceptedFiles: '.mp4,.m3u8'
        },
    directory_name: 'movies_upload/url_720'
	});

  $(".midia-toggle-upload_video_1080").midia({
		base_url: '{{url('')}}',
    dropzone : {
          acceptedFiles: '.mp4,.m3u8'
        },
    directory_name: 'movies_upload/url_1080'
	});
</script>
<script type="text/javascript" src="{{url('js/encrypt.js')}}"></script> <!-- bootstrap js -->
<script>
  $(document).ready(function() {
    $apicurrentValue = $('#apiUrl').val();
    if($apicurrentValue.includes('encrypt:')){
      //console.log('true');
      $('#encryptlink').hide();
    }else{
      //console.log('false');
      $('#encryptlink').show();
    }
  });

  $('#apiUrl').on('change',function(){
    $apicurrentValue = $('#apiUrl').val();
    if($apicurrentValue.includes('encrypt:')){
      //console.log('true');
      $('#encryptlink').hide();
    }else{
      //console.log('false');
      $('#encryptlink').show();
    }
  });
</script>   
@endsection