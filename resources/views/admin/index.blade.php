@extends('layouts.master')
@section('title',__('Dashboard'))
@section('maincontent')
@section('breadcum')
<div class="breadcrumbbar">
          <h4 class="page-title">{{ __('Home') }}</h4>
          <div class="breadcrumb-list">
              <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{ url('/') }}">{{ __('Home') }}</a></li>
                  <li class="breadcrumb-item active">{{ __('Dashboard') }}</li>

              </ol>
          </div>
</div>
@endsection
<div class="contentbar">
  @can('dashboard.states')
      <!-- Start row -->
      <div class="row">
          <!-- Start col -->
          <div class="col-lg-12">

              <!--<div class="alert alert-success alert-dismissible fade show">-->
          
          
              <!--    <span id="update_text">-->
                  
              <!--    </span>-->
  
              <!--    <form action="{{ url("/admin/merge-quick-update") }}" method="POST" class="float-right d-none updaterform">-->
              <!--        @csrf-->
              <!--        <input required type="hidden" value="" name="filename">-->
              <!--        <input required type="hidden" value="" name="version">-->
              <!--        <button class="btn btn-sm btn-primary-rgba">-->
              <!--        <i class="feather icon-check-circle"></i> {{__("Update Now")}}-->
              <!--        </button>-->
              <!--    </form>-->
                  
              <!--    <button type="button" class="close" data-dismiss="alert" aria-label="Close">-->
              <!--    <span aria-hidden="true">&times;</span>-->
              <!--    </button>-->
  
              <!--</div>-->
          </div>

          <div class="col-lg-12">

              <!-- Start row -->
              <div class="row">

                  <!-- Start col -->
                  <div class="col-lg-3 col-md-6 col-12">
                      <div class="card m-b-30 shadow-sm">
                          <div class="card-body">
                              <div class="row align-items-center">
                                  <div class="col-6">
                                      <h4>{{$users_count}}</h4>
                                      <p class="font-14 mb-0">{{ __('Usuários') }}</p>
                                  </div> 
                              <div class="col-6 text-right">
                                  <a href="{{url('admin/users')}}"><i
                                      class="text-info feather icon-users icondashboard"></i></a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-3 col-md-6 col-12">
                      <div class="card m-b-30 shadow-sm">
                          <div class="card-body">
                              <div class="row align-items-center">
                                  <div class="col-6">
                                      <h4>{{ $activeusers }}</h4>
                                      <p class="font-14 mb-0">{{ __('Usuários ativos') }}</p>
                                  </div>
                                  <div class="col-6 text-right">
                                      <a href="{{url('admin/users')}}"><i
                                          class="text-warning feather icon-user icondashboard"></i></a>
                                      </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-3 col-md-6 col-12">
                      <div class="card m-b-30 shadow-sm">
                          <div class="card-body">
                              <div class="row align-items-center">
                                  <div class="col-6">
                                      <h4>{{$movies_count}}</h4>
                                      <p class="font-12 mb-0">{{ __('Total de videos') }}</p>
                                  </div>
                                  <div class="col-6 text-right">
                                      <a href="{{url('admin/movies')}}"><i
                                          class="text-success feather icon-video icondashboard"></i></a>
                                      </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-3 col-md-6 col-12 mt-md-3">
                      <div class="card m-b-30 shadow-sm">
                          <div class="card-body">
                              <div class="row align-items-center">
                                  <div class="col-6">
                                      <h4>{{$package_count}}</h4>
                                      <p class="font-14 mb-0">{{__('Total de planos')}}</p>
                                  </div>
                                  <div class="col-6 text-right">
                                      <a href="{{url('admin/packages')}}"><i
                                          class="text-success feather icon-shopping-bag icondashboard"></i></a>
                                      </div>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="col-lg-3 col-md-6 col-12 mt-md-3">
                      <div class="card m-b-30 shadow-sm">
                          <div class="card-body">
                              <div class="row align-items-center">
                                  <div class="col-6">
                                      <h4>{{$coupon_count}}</h4>
                                      <p class="font-12 mb-0">{{__('Total de cupons')}}</p>
                                  </div>
                                  <div class="col-6 text-right">
                                      <a href="{{url('admin/coupons')}}"><i
                                          class="text-success feather icon-percent icondashboard"></i></a>
                                      </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  
                  <div class="col-lg-3 col-md-6 col-12 mt-md-3">
                      <div class="card m-b-30 shadow-sm">
                          <div class="card-body">
                              <div class="row align-items-center">
                                  <div class="col-6">
                                      <h4>{{$genres_count}}</h4>
                                      <p class="font-12 mb-0">{{__('Tipos de faixas')}}</p>
                                  </div>
                                  <div class="col-6 text-right">
                                      <a href="{{url('admin/genres')}}"><i
                                          class="text-warning feather icon-radio icondashboard"></i></a>
                                  </div>
                                  
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-3 col-md-6 col-12">
                    <div class="card m-b-30 shadow-sm">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <h4>{{$movieslw}}</h4>
                                    <p class="font-12 mb-0">{{ __('Videos Top 10') }}</p>
                                </div>
                                <div class="col-6 text-right">
                                    <a href="{{url('admin/topmovies')}}"><i
                                        class="text-success feather icon-video icondashboard"></i></a>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>

              <!-- Start row -->
              <div class="row">
                <!-- Start Active Subscribed User-->
                <div class="col-lg-12 col-xl-7">
                    <div class="card m-b-30">
                        <div class="card-header">                                
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <h5 class="card-title mb-0">{{__('Aquisições de assinaturas em ' . date('Y'))}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! $activesubsriber->container() !!}
                        </div>
                    </div>
                </div>
                <!-- End Active Subscribed User -->
                <!-- Start User Distribution -->
                <div class="col-lg-12 col-xl-5">
                    <div class="card m-b-30">
                        <div class="card-header">                                
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <h5 class="card-title mb-0">{{__('Usuários ativos/inativos e assinantes ativos')}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! $piechart->container() !!}
                        </div>
                    </div>
                </div>
                <!-- End User Distribution -->
            </div>
            <!-- End row -->

            <!-- Start Revenue Report -->
            <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header">                                
                        <div class="row align-items-center">
                            <div class="col-9">
                                <h5 class="card-title mb-0">{{__('Histórico de assinaturas')}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                      <div class="table-responsive">
                        <table id="devicetable" class="table table-borderd">
                          <thead>
                            <th> {{ __('#') }}</th>
                            <th>{{__('ID')}}</th>
                            <th>{{__('Nome')}}</th>
                            <th>{{__('Pagamento')}}</th>
                            <th>{{__('Valor pago')}}</th>
                            <th>{{__('Assinado em')}}</th>
                            <th>{{__('Assinante até')}}</th>
                          </thead>
                            @if ($revenue_report)
                                <tbody>
                                    @foreach ($revenue_report as $key => $report)
                                        <tr id="item-{{$report->id}}">
                                        <td>{{$key+1}}</td>
                                        <td>{{$report->payment_id}}</td>
                                        <td>{{ucfirst($report->user_name)}}</td>
                                        <td>{{$report->method}}</td>
                                        <td><i class="{{ $currency_symbol }}" aria-hidden="true"></i>{{$report->price}}</td>
                                        <td>{{$report->subscription_from}}</td>
                                        <td>{{$report->subscription_to}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            @endif
                        </table>                  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <!-- End Revenue Report -->

              <!-- Start row -->
              <div class="row">
                <!-- Start Monthly Registered Users -->
                <div class="col-md-12">
                    <div class="card m-b-30">
                        <div class="card-header">                                
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <h5 class="card-title mb-0">{{__('Usuários cadastrados em' . date('Y'))}}}</h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! $userchart->container() !!}
                        </div>
                    </div>
                </div>
                <!-- End Monthly Registered Users -->
            </div>
            <!-- End row -->

              <div class="row">

                <!-- Start Paypal Subscription -->
                <div class="col-lg-12 col-xl-12">
                    <div class="card m-b-30">
                        <div class="card-body">
                            {!! $revenue_chart->container() !!}
                        </div>
                    </div>
                </div>
                <!-- End Paypal Subscription -->

                
          </div>

@endcan
  </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $userchart->script() !!}
{!! $activesubsriber->script() !!}
{!! $piechart->script() !!}
{!! $doughnutchart->script() !!}
{!! $revenue_chart->script() !!}
<script>
    console.clear();
</script>
@endsection

