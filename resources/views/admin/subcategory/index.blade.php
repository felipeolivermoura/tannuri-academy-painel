@extends('layouts.master')
@section('title',__('Todas subcategorias'))
@section('breadcum')
	<div class="breadcrumbbar">
                <h4 class="page-title">{{ __('Todas subcategorias') }}</h4>
                <div class="breadcrumb-list">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{url('/admin')}}">{{ __('Dashboard') }}</a></li>
                      <li class="breadcrumb-item active" aria-current="page">{{ __('Subcategorias') }}</li>
                    </ol>
                </div> 
    </div>
@endsection
@section('maincontent')
<div class="contentbar"> 
    <div class="row">
        <div class="col-md-12">

            <div class="card m-b-50">
                <div class="card-header">
                    <button type="button" class="float-right btn btn-danger-rgba mr-2 " data-toggle="modal"
            data-target="#bulk_delete"><i class="feather icon-trash mr-2"></i> {{ __('Apagar selecionados') }} </button>
            <a href="{{route('sub_category.create')}}" class="float-right btn btn-primary-rgba mr-2"><i
                class="feather icon-plus mr-2"></i>{{ __('Add Subcategorias') }} </a>
                    <h5 class="card-title">{{ __('Todas subcategorias') }}</h5>
                    
                </div> 

                <div class="card-body">
                    <div class="table-responsive">
                         <table id="categoryTable" class="table table-borderd">

                            <thead>
                              <th>
                                <div class="inline">
                                  <input id="checkboxAll" type="checkbox" class="filled-in" name="checked[]" value="all" id="checkboxAll">
                                  <label for="checkboxAll" class="material-checkbox"></label>
                                </div>
                              </th>
                              <th>{{__('Titulo')}}</th>
                              <th>{{__('Criado em')}}</th>
                              <th>{{__('Atualizado em')}}</th>
                              <th>{{__('Ações')}}</th>
                            </thead>

                            @if (isset($categories))
                              <tbody>
                              
                              </tbody>
                            @endif

                            <div id="bulk_delete" class="delete-modal modal fade" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close"
                                                data-dismiss="modal">&times;</button>
                                            <div class="delete-icon"></div>
                                        </div>
                                        <div class="modal-body text-center">
                                            <h4 class="modal-heading">{{__('Você tem certeza?')}}</h4>
                                            <p>{{__('Esta ação não poderá ser desfeita. Deseja continuar?')}}</p>
                                        </div>
                                        <div class="modal-footer">
                                          {!! Form::open(['method' => 'POST', 'action' => 'SubCategoryController@bulk_delete', 'id' => 'bulk_delete_form']) !!}
                                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">{{__('Não')}}</button>
                                                <button type="submit" class="btn btn-danger">{{__('Sim')}}</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        

                        </table>                  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('script')
   
<script>
  $(function(){
    $('#checkboxAll').on('change', function(){
      if($(this).prop("checked") == true){
        $('.material-checkbox-input').attr('checked', true);
      }
      else if($(this).prop("checked") == false){
        $('.material-checkbox-input').attr('checked', false);
      }
    });
  });
</script>
 <script>
  $(function () {
    "use strict";
    jQuery.noConflict();
    var table;
    if($.fn.dataTable.isDataTable('#categoryTable')){
      table = $('#categoryTable').DataTable();
    }else{
      table = $('#categoryTable').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        autoWidth: false,
        scrollCollapse: true,
        ajax: "{{ route('sub_category.index') }}",
        columns: [
            {data: 'checkbox', name: 'checkbox',orderable: false, searchable: false},
            {data: 'title', name: 'title'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'action',searchable: false}
           
        ],
        dom : 'lBfrtip',
        buttons : [
          'csv','excel','pdf','print'
        ]
      });
    }
      
  });

   
</script>
@endsection