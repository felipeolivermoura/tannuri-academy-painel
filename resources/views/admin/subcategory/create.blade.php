@extends('layouts.master')
@section('title',__('Criar subcategoria'))
@section('breadcum')
	<div class="breadcrumbbar">
                <h4 class="page-title">{{ __('HOME') }}</h4>
                <div class="breadcrumb-list">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{url('/admin')}}">{{ __('Dashboard') }}</a></li>
                      <li class="breadcrumb-item active" aria-current="page">{{ __('Criar subcategoria') }}</li>
                    </ol>
                </div>  
    </div>
@endsection
@section('maincontent')
<div class="contentbar">
  <div class="row">
    @if ($errors->any())  
  <div class="alert alert-danger" role="alert">
  @foreach($errors->all() as $error)     
  <p>{{ $error}}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true" style="color:red;">&times;</span></button></p>
      @endforeach  
  </div>
  @endif
    <div class="col-md-12">
      <div class="card m-b-50">
        <div class="card-header">
          <a href="{{url('admin/label')}}" class="float-right btn btn-primary-rgba mr-2"><i
            class="feather icon-arrow-left mr-2"></i>{{ __('Voltar') }}</a>
          <h5 class="box-title">{{__('Criar subcategoria')}}</h5>
        </div>
        <div class="card-body ml-2">
          {!! Form::open(['method' => 'POST', 'action' => 'SubCategoryController@store']) !!}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group text-dark{{ $errors->has('title') ? ' has-error' : '' }}">
                  {!! Form::label('title', __('Titulo')) !!}
                  {!! Form::text('title', old('title'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => __('Insira o titulo da subcategoria')]) !!}
                  <small class="text-danger">{{ $errors->first('title') }}</small>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group text-dark{{ $errors->has('category_id') ? ' has-error' : '' }}">
                  {!! Form::label('category_id', __('Categoria')) !!}
                  {!! Form::select('category_id', $categories, null, ['class' => 'form-control select2', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('category_id') }}</small>
              </div>
            </div>
          </div>
              
              
                <div class="form-group">
                  <button type="reset" class="btn btn-success-rgba">{{__('Redefinir')}}</button>
                  <button type="submit" class="btn btn-primary-rgba"><i class="fa fa-check-circle"></i>
                    {{ __('Salvar') }}</button>
                </div>
                {!! Form::close() !!}
                <div class="clear-both"></div>
            

      </div>
    </div>
  </div>
</div>
</div>
@endsection 
@section('script')
@endsection