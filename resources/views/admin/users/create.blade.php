@extends('layouts.master')
@section('title',__('Criar usuário'))
@section('breadcum')
	<div class="breadcrumbbar">
                <h4 class="page-title">{{ __('HOME') }}</h4>
                <div class="breadcrumb-list">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{url('/admin')}}">{{ __('Dashboard') }}</a></li>
                      <li class="breadcrumb-item active" aria-current="page">{{ __('Criar usuário') }}</li>
                    </ol>
                </div>  
    </div>
@endsection
@section('maincontent')
<div class="contentbar">
  <div class="row">
    @if ($errors->any())  
  <div class="alert alert-danger" role="alert">
  @foreach($errors->all() as $error)     
  <p>{{ $error}}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true" style="color:red;">&times;</span></button></p>
      @endforeach  
  </div>
  @endif
    <div class="col-lg-12">
      <div class="card m-b-50">
        <div class="card-header">
          <a href="{{ route('users.index') }}" class="float-right btn btn-primary-rgba mr-2"><i
            class="feather icon-arrow-left mr-2"></i>{{ __('Voltar') }}</a>
          <h5 class="box-title">{{__('Criar usuário')}}</h5>
        </div>
        <div class="card-body ml-2">
          {!! Form::open(['method' => 'POST', 'action' => 'UsersController@store', 'files' => true]) !!}
          <div class="bg-info-rgba p-4 mb-4">
            <h4 class="pb-4">Dados pessoais</h4>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="name">
                    {{ __('Insira o nome') }}:
                    <sup class="text-danger">*</sup>
                  </label>
                  <input value="{{ old('name') }}" autofocus required name="name" type="text" class="form-control"
                    placeholder="{{ __('Digite o nome') }}" />
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="track_type">{{__('Belt')}}</label>
                    <select id="track_type" class="form-control select2" name="track_type">
                      <option value="">{{__('Belt')}}</option>
                      <option value="0" @if(old('track_type') == '0') selected @endif>White</option>
                      <option value="1" @if(old('track_type') == '1') selected @endif>Yellow</option>
                      <option value="2" @if(old('track_type') == '2') selected @endif>Orange</option>
                      <option value="3" @if(old('track_type') == '3') selected @endif>Green</option>
                      <option value="4" @if(old('track_type') == '4') selected @endif>Blue</option>
                      <option value="5" @if(old('track_type') == '5') selected @endif>Purple</option>
                      <option value="6" @if(old('track_type') == '6') selected @endif>Brown</option>
                      <option value="7" @if(old('track_type') == '7') selected @endif>Black</option>
                    </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="role">{{ __('Tipo de função') }}:</label>
                  <select class="form-control select2" name="role">
                    @foreach($roles as $role)
                    <option vlaue="{{$role->name}}">{{ucfirst($role->name)}}</option>
                  @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">{{ __('Email') }}:<sup class="text-danger">*</sup> </label>
                  <input value="{{ old('email') }}" required type="email" name="email"
                    placeholder="{{ __('ex: exemplo@gmail.com') }}"
                    class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="mobile"> {{ __('Telefone') }}:</label>
                  <input value="{{ old('mobile') }}" type="number" name="mobile"
                    placeholder="{{ __('Digite o telefone') }} "
                    class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>{{ __('Senha') }}</label>
                  <input type="password" name="password" class="form-control"
                    placeholder="{{ __('Digite a senha') }}">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" >
                  <label>{{ __('Confirme a senha') }}</label>
                  <input type="password" name="confirm_password" class="form-control"
                    placeholder="{{ __('Digite a senha novamente') }}">
                </div>
              <!--</div>-->
              <!--<div class="col-md-6">-->
              <!--  <div class="form-group">-->
              <!--    <label for="age"> {{ __('Age') }}:</label>-->
              <!--    <input value="{{ old('age') }}" type="number" name="age"-->
              <!--      placeholder="{{ __('Please enter your Age') }} "-->
              <!--      class="form-control">-->
              <!--  </div>-->
              <!--</div>-->
            </div>
          </div>
          <!--<div class="bg-info-rgba p-4 mb-4">-->
          <!--  <h4 class="pb-4">Address</h4>-->
          <!--  <div class="row">-->
          <!--    <div class="col-md-6">-->
          <!--      <div class="form-group">-->
          <!--        <label for="address">{{ __('Address') }}: </label>-->
          <!--        <textarea name="address" class="form-control" rows="1"-->
          <!--          placeholder="{{ __('Please Enter Your Address') }}" value="{{ old('address') }}"></textarea>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--    <div class="col-md-6">-->
          <!--      <div class="form-group">-->
          <!--        <label for="country">{{__('Country')}}</label>-->
          <!--        <select id="country-dropdown" class="form-control select2" name="country">-->
          <!--          <option value="">{{__('Select Your Country')}}</option>-->
          <!--          @foreach ($country as $country) -->
          <!--          <option value="{{$country->id}}">-->
          <!--          {{$country->name}}-->
          <!--          </option>-->
          <!--          @endforeach-->
          <!--        </select>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--    <div class="col-md-6">-->
          <!--      <div class="form-group">-->
                  
          <!--        <label for="state">{{__('State')}}:</label>-->
          <!--        <select class="form-control select2"  name="state" id="state-dropdown"  >-->
          <!--        </select>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--    <div class="col-md-6">-->
          <!--      <div class="form-group">-->
          <!--        <label for="city">{{__('City')}}</label>-->
          <!--        <select class="form-control select2"  name="city" id="city-dropdown" >-->
          <!--        </select>-->
          <!--      </div>-->
          <!--    </div>-->
          <!--  </div>-->
          <!--</div>-->
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <button type="reset" class="btn btn-success-rgba">{{__('Resetar')}}</button>
                <button type="submit" class="btn btn-primary-rgba"><i class="fa fa-check-circle"></i>
                  {{ __('Cadastrar') }}</button>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
          <div class="clear-both"></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection 
@section('script')
<script>
  (function($){
    $.noConflict();
    $('form').on('submit', function(event){
      $('.loading-block').addClass('active');
    });

    $(".toggle-password2").click(function() {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
  })(jQuery);

  
  
</script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script>
      (function($) {
        jQuery.noConflict();
      $('#country-dropdown').on('change', function() {
      var country_id = this.value;
      $("#state-dropdown").html('');
      $.ajax({
      url:"{{url('get-states-by-country')}}",
      type: "POST",
      data: {
      country_id: country_id,
      _token: '{{csrf_token()}}' 
      },
      dataType : 'json',
      success: function(result){
        console.log("Result is ", result);
      $('#state-dropdown').html('<option value="">Select State</option>'); 
      $.each(result.states,function(key,value){
      $("#state-dropdown").append('<option value="'+value.id+'">'+value.name+'</option>');
      });
      $('#city-dropdown').html('<option value="">Select State First</option>'); 
      }
      });
      });    
      $('#state-dropdown').on('change', function() {
        $.noConflict();
      var state_id = this.value;
      $("#city-dropdown").html('');
      $.ajax({
      url:"{{url('get-cities-by-state')}}",
      type: "POST",
      data: {
      state_id: state_id,
      _token: '{{csrf_token()}}' 
      },
      dataType : 'json',
      success: function(result){
      $('#city-dropdown').html('<option value="">Select City</option>'); 
      $.each(result.cities,function(key,value){
      $("#city-dropdown").append('<option value="'+value.id+'">'+value.name+'</option>');
      });
      }
      });
      });
      })(jQuery);
</script>
    
@endsection