<p><b>{{ __('Nome') }}</b>: {{ $name }}</p>
<p><b>{{ __('Email') }}</b>: {{ $email }}</p>
<p><b>{{ __('Telefone') }}</b>: @if(isset($mobile))
    {{ $mobile }}
    @endif</p>