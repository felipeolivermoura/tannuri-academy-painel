@extends('layouts.master')
@section('title',__('Alterar assinatura')." ". $user->name)
@section('breadcum')
	<div class="breadcrumbbar">
    <h4 class="page-title">{{ __('HOME') }}</h4>
    <div class="breadcrumb-list">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/admin')}}">{{ __('Dashboard') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ __('Alterar plano de assinatura') }}</li>
      </ol>
    </div>     
  </div>
@endsection
@section('maincontent')
<div class="contentbar">
  <div class="row">
    @if ($errors->any())  
    <div class="alert alert-danger" role="alert">
      @foreach($errors->all() as $error)     
        <p>{{ $error}}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" style="color:red;">&times;</span></button></p>
      @endforeach  
    </div>
    @endif
    <div class="col-lg-12">
      <div class="card m-b-30">
        <div class="card-header">
          <a href="{{ route('users.index') }}" class="float-right btn btn-primary-rgba mr-2"><i
            class="feather icon-arrow-left mr-2"></i>{{ __('Voltar') }}</a>
          <h5 class="box-title">{{__('Altere ou adicione o plano de assinatura')}}</h5>
        </div>
        <div class="card-body ml-2">
          {!! Form::open(['method' => 'POST', 'action' => 'UsersController@change_subscription', 'files' => true]) !!}
          <div class="change-subs-title mb-3">
            <h5>{{__('Nome do usuário')}}: {{$user->name}}</h5>
            @php
              $planname='Não existe';
              if (isset($plans)) {
                if (isset($last_payment->plan->name) && !is_null($last_payment)){
                    $planname=$last_payment->plan->name;
                    $planid = $last_payment->plan->id;
                  }else{
                  if (isset($user_stripe_plan) && !is_null($user_stripe_plan)) {
                    $planname=$user_stripe_plan->name;
                    $planid = $user_stripe_plan->id;
                  }
                }
              }
              else{
                  $planname='Não existe';
              }

            @endphp
              
            <h5>{{__('Último Plano de Assinatura')}}: {{$planname}}</h5>
          </div>
          <input type="hidden" name="user_stripe_plan_id" value="{{$user_stripe_plan != null ? $user_stripe_plan->id : null}}">
          <input type="hidden" name="last_payment_id" value="{{$last_payment != null ? $last_payment->id : null}}">
          <input type="hidden" name="user_id" value="{{$user->id}}">


          @foreach ($user->paypal_subscriptions as $pu)
            @php
              $test=0;
              $status =App\Package::select('status')->where('id',$pu->package_id)->get();
                    foreach ($status as $key => $value) {
                    $test=$value->status;
                    }
            @endphp

            @if($test == 1)
              <div class="alert alert-danger">
                {{__('Assinatura de usuário não existe')}}
              </div>
            @endif
          @endforeach
          <div class="form-group">
            <select class="form-control select2" name="plan_id">
              @foreach ($plans as $plan)
              @if($plan->delete_status == 1)
              <option value="{{ $plan->id }}" {{isset($planid) && $planid == $plan->id ? 'Selecionado' : ''}}>{{ $plan->name }}</option>
              @endif
            @endforeach
            </select>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-success-rgba"><i class="fa fa-check-circle mr-1"></i>
              {{ __('Confirmar alterações') }}</button>
          </div>
          {!! Form::close() !!}
          <div class="clear-both"></div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>

@endsection 