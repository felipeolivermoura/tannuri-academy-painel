@extends('layouts.master')
@section('title',__('Recursos dos planos'))
@section('breadcum')
	<div class="breadcrumbbar">
                <h4 class="page-title">{{ __('Recursos dos planos') }}</h4>
                <div class="breadcrumb-list">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="{{url('/admin')}}">{{ __('Dashboard') }}</a></li>
                      <li class="breadcrumb-item active" aria-current="page">{{ __('Recursos dos planos') }}</li>
                    </ol>
                </div>   
    </div>
@endsection
@section('maincontent')
<div class="contentbar"> 
    <div class="row">
        <div class="col-md-12">

            <div class="card m-b-50">
                <div class="card-header">
            @can('package-feature.delete')
            <button type="button" class="float-right btn btn-danger-rgba mr-2 " data-toggle="modal"
            data-target="#bulk_delete"><i class="feather icon-trash mr-2"></i> {{ __('Remover Selecionados') }} </button>
            @endcan
            @can('package-feature.create')
            <a href="{{route('package_feature.create')}}" class="float-right btn btn-primary-rgba mr-2"><i
                class="feather icon-plus mr-2"></i>{{ __('Criar Recurso') }} </a>
              @endcan
                    <h5 class="card-title">{{ __('Recursos dos planos') }}</h5>
                    
                </div> 

                <div class="card-body">
                    <div class="table-responsive">
                         <table id="package_featureTable" class="table table-borderd">

                            <thead>
                                <th> {{ __('#') }}</th>
                                <th> {{ __('NOME') }}</th>
                                <th> {{ __('CRIADO EM') }}</th>
                                <th> {{ __('ATUALIZADO EM') }}</th>
                                <th> {{ __('AÇOES') }}</th>
                            </thead>

                            @if ($p_feature)
                            <tbody>
                         
                            </tbody>
                          @endif

                            <div id="bulk_delete" class="delete-modal modal fade" role="dialog">
                                <div class="modal-dialog modal-sm">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close"
                                                data-dismiss="modal">&times;</button>
                                            <div class="delete-icon"></div>
                                        </div>
                                        <div class="modal-body text-center">
                                            <h4 class="modal-heading">{{__('Você tem certeza?')}}</h4>
                                            <p>{{__('Você tem certeza que deseja realmente remover este dado? Esta ação não pode ser desfeita.')}}</p>
                                        </div>
                                        <div class="modal-footer">
                                            {!! Form::open(['method' => 'POST', 'action' => 'PackageFeatureController@bulk_delete', 'id' => 'bulk_delete_form']) !!}
                                                @method('POST')
                                                <button type="reset" class="btn btn-gray translate-y-3" data-dismiss="modal">{{__('Não')}}</button>
                                                <button type="submit" class="btn btn-danger">{{__('Sim')}}</button>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </table>                  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
@section('script')
   
<script>
  $(function(){
    $('#checkboxAll').on('change', function(){
      if($(this).prop("checked") == true){
        $('.material-checkbox-input').attr('checked', true);
      }
      else if($(this).prop("checked") == false){
        $('.material-checkbox-input').attr('checked', false);
      }
    });
  });
</script>
 <script>
  $(function () {
    jQuery.noConflict();
    var table;
    if ( $.fn.dataTable.isDataTable( '#package_featureTable' ) ) {
        table = $('#package_featureTable').DataTable()
    }else{
        var table = $('#package_featureTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            autoWidth: false,
            scrollCollapse: true,
     
       
            ajax: "{{ route('package_feature.index') }}",
            columns: [
                {data: 'checkbox', name: 'checkbox',orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'action', name: 'action',searchable: false}
            
            ],
            dom : 'lBfrtip',
            buttons : [
            'csv','excel','pdf','print'
            ],
            order : [[0,'desc']]
        });    
    }
    // var table = $('#package_featureTable').DataTable({
    //     processing: true,
    //     serverSide: true,
    //    responsive: true,
    //    autoWidth: false,
    //    scrollCollapse: true,
     
       
    //     ajax: "{{ route('package_feature.index') }}",
    //     columns: [
    //         {data: 'checkbox', name: 'checkbox',orderable: false, searchable: false},
    //         {data: 'name', name: 'name'},
    //         {data: 'created_at', name: 'created_at'},
    //          {data: 'updated_at', name: 'updated_at'},
    //         {data: 'action', name: 'action',searchable: false}
           
    //     ],
    //     dom : 'lBfrtip',
    //     buttons : [
    //       'csv','excel','pdf','print'
    //     ],
    //     order : [[0,'desc']]
    // });
    
  });
</script>
@endsection