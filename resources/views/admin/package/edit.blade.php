@extends('layouts.master')
@section('title', __('Editar plano'))
@section('breadcum')
    <div class="breadcrumbbar">
        <h4 class="page-title">{{ __('HOME') }}</h4>
        <div class="breadcrumb-list">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url('/admin') }}">{{ __('Dashboard') }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ __('Editar plano') }}</li>
            </ol>
        </div>
    </div>
@endsection
@section('maincontent')
    <div class="contentbar">
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger" role="alert">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true" style="color:red;">&times;</span></button></p>
                    @endforeach
                </div>
            @endif
            <div class="col-md-12">
                <div class="card m-b-50">
                    <div class="card-header">
                        <a href="{{ url('admin/packages') }}" class="float-right btn btn-primary-rgba mr-2"><i
                                class="feather icon-arrow-left mr-2"></i>{{ __('Voltar') }}</a>
                        <h5 class="box-title">{{ __('Editar plano') }}</h5>
                    </div>
                    <div class="card-body ml-2">
                        {!! Form::model($package, ['method' => 'PATCH', 'action' => ['PackageController@update', $package->id]]) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group text-dark{{ $errors->has('plan_id') ? ' has-error' : '' }}">
                                    {!! Form::label('plan_id', __('PlanID')) !!}
                                    {!! Form::text('plan_id', $package->plan_id, [
                                        'class' => 'form-control',
                                        'required' => 'required',
                                        'data-toggle' => 'popover',
                                        'data-content' => __('Unique Package') . ' ex. basic10',
                                        'data-placement' => 'bottom',
                                    ]) !!}
                                    <small class="text-danger">{{ $errors->first('plan_id') }}</small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group text-dark{{ $errors->has('name') ? ' has-error' : '' }}">
                                    {!! Form::label('name', __('Nome do plano')) !!}
                                    {!! Form::text('name', $package->name, ['class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('name') }}</small>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group text-dark{{ $errors->has('interval') ? ' has-error' : '' }}">
                                    {!! Form::label('interval', __('Recorrência')) !!}
                                    {!! Form::select(
                                        'interval',
                                        ['day' => 'Diário', 'week' => 'Semanal', 'month' => 'Mensal', 'year' => 'Anual'],
                                        $package->interval,
                                        ['class' => 'form-control select2', 'required' => 'required'],
                                    ) !!}
                                    <small class="text-danger">{{ $errors->first('interval') }}</small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group text-dark{{ $errors->has('interval_count') ? ' has-error' : '' }}">
                                    {!! Form::label('interval_count', __('Duração do plano')) !!}
                                    {!! Form::number('interval_count', $package->interval_count, ['min' => 1, 'class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('interval_count') }}</small>
                                </div>
                            </div>

                            {!! Form::hidden('currency', $currency_code) !!}
                            <div class="col-md-4">
                                <div class="form-group text-dark{{ $errors->has('free') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::label('free', __('É grátis?')) !!}
                                        </div>
                                        <div class="col-md-5 pad-0">
                                            <label class="switch">
                                                {!! Form::checkbox('free', 1, $package->free, ['class' => 'custom_toggle seriescheck', 'id' => 'free']) !!}
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('free') }}</small>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group text-dark{{ $errors->has('amount') ? ' has-error' : '' }}"
                                    style="{{ $package->free == 1 ? 'display: none' : '' }}" id="planAmount">
                                    {!! Form::label('amount', __('Valor do plano')) !!}
                                    <div class="input-group">
                                        <span class="input-group-addon simple-input"><i
                                                class="{{ $currency_symbol }}"></i></span>
                                        {!! Form::number('amount', null, ['class' => 'form-control', 'step' => '0.01']) !!}
                                    </div>
                                    @if ($package->currency_symbol == '')
                                        <input type="text" name="currency_symbol" id="currency_symbol"
                                            value="{{ $currency_symbol }}" hidden="true">
                                    @else
                                        <input type="text" name="currency_symbol" id="currency_symbol"
                                            value="{{ $package->currency_symbol }}" hidden="true">
                                    @endif
                                    <small class="text-danger">{{ $errors->first('valor') }}</small>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group text-dark{{ $errors->has('download') ? ' has-error' : '' }}">
                                    <div class="row">
                                        <div class="col-md-6">
                                            {!! Form::label('download', __('Você deseja limitar downloads?')) !!}
                                        </div>
                                        <div class="col-md-5 pad-0">
                                            <label class="switch">
                                                {!! Form::checkbox('download', 1, $package->download, [
                                                    'class' => 'custom_toggle seriescheck',
                                                    'id' => 'download_enable',
                                                ]) !!}
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <small class="text-danger">{{ $errors->first('download') }}</small>
                                    </div>
                                </div>
                                <small class="text-danger">{{ $errors->first('downloadlimit') }}</small>
                                <div id="downloadlimit"
                                    class="form-group{{ $errors->has('downloadlimit') ? ' has-error' : '' }}"
                                    style="{{ $package->download ? '' : 'display:none' }}">
                                    {!! Form::label('downloadlimit', __('Limite')) !!}
                                    {!! Form::number('download limit', null, ['class' => 'form-control']) !!}
                                    <small class="text-danger">{{ __('Atenção') }} :- <br />
                                        1. {{ __('O limite de download será aplicado por perfil') }}.<br />
                                        2.
                                        {{ __('Se você inserir 0, significa que o usuário pode fazer download ilimitado') }}
                                    </small>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group text-dark{{ $errors->has('genre_id') ? ' has-error' : '' }}">
                                    {!! Form::label('genre_id', __('Tipos de faixas')) !!}<span class="text-danger">*</span>
                                    {!! Form::select('genre_id[]', $genres, $p_genres, ['class' => 'form-control select2', 'multiple']) !!}
                                    <small class="text-danger">{{ $errors->first('genre_id') }}</small>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group text-dark{{ $errors->has('feature') ? ' has-error' : '' }}">
                                    {!! Form::label('feature', __('Features')) !!}<span class="text-danger">*</span>
                                    <select name="feature[]" class="select2 form-control" multiple>
                                        @foreach ($p_feature as $pf)
                                            <option
                                                @isset($package['feature']) @foreach ($package['feature'] as $opf) {{ $opf == $pf['id'] ? 'selected' : '' }}  @endforeach @endisset
                                                value="{{ $pf->id }}">{{ $pf->name }} </option>
                                        @endforeach

                                    </select>
                                    <small class="text-danger">{{ $errors->first('feature') }}</small>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group text-dark{{ $errors->has('status') ? ' has-error' : '' }}">
                                    {!! Form::label('status', __('Status')) !!}<span class="text-danger">*</span>
                                    <select name="status" class="select2 form-control">
                                        <option value="active" {{ $package->status == 'active' ? 'selected' : '' }}>
                                            {{ __('Ativo') }}</option>
                                        <option value="upcoming" {{ $package->status == 'upcoming' ? 'selected' : '' }}>
                                            {{ __('Pausado') }}</option>
                                        <option value="inactive" {{ $package->status == 'inactive' ? 'selected' : '' }}>
                                            {{ __('Inativo') }}</option>
                                    </select>
                                    <small class="text-danger">{{ $errors->first('status') }}</small>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div
                                    class="form-group text-dark{{ $errors->has('trial_period_days') ? ' has-error' : '' }}">
                                    {!! Form::label('trial_period_days', __('Defina o tempo de teste em dias')) !!}
                                    {!! Form::number('trial_period_days', $package->trial_period_days, ['class' => 'form-control']) !!}
                                    <small class="text-danger">{{ $errors->first('trial_period_days') }}</small>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="menu-block">
                                    <h6 class="menu-block-heading">{{ __('Selecione o menu que será exibido') }}</h6>

                                    @if (isset($menus) && count($menus) > 0)
                                        <div class="row">
                                            <div class="col-md-4">
                                                {{ __('Todos') }}
                                            </div>
                                            <div class="col-md-5 pad-0">
                                                <div class="inline">
                                                    <input type="checkbox" class=" filled-in material-checkbox-input all "
                                                        name="menu[]" value="100" id="checkbox{{ 100 }}">
                                                    <label for="checkbox{{ 100 }}" class="custom_toggle"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            @foreach ($menus as $menu)
                                                <div class="col-md-4">
                                                    {{ $menu->name }}
                                                </div>
                                                <div class="col-md-5 pad-0">
                                                    <div class="inline">
                                                        <input
                                                            @foreach ($menu->getpackage as $pkg) {{ $pkg->menu_id == $menu->id && $package->id == $pkg->pkg_id ? 'checked' : '' }} @endforeach
                                                            type="checkbox" class="filled-in material-checkbox-input one "
                                                            name="menu[]" value="{{ $menu->id }}"
                                                            id="checkbox{{ $menu->id }}">
                                                        <label for="checkbox{{ $menu->id }}"
                                                            class="custom_toggle"></label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mt-4 ml-4">
                        <button type="reset" class="btn btn-success-rgba">{{ __('Resetar') }}</button>
                        <button type="submit" class="btn btn-primary-rgba"><i class="fa fa-check-circle"></i>
                            {{ __('Atualizar') }}</button>
                    </div>
                    {!! Form::close() !!}
                    <div class="clear-both"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                var check = [];

                $('.one').each(function(index) {

                    if ($(this).is(':checked')) {
                        check.push(1);
                    } else {
                        check.push(0);
                    }

                });
                console.log(check);
                var flag = 1;

                if (jQuery.inArray(0, check) == -1) {
                    flag = 1;

                } else {
                    flag = 0;
                }

                if (flag == 0) {
                    $('.all').prop('checked', false);
                } else {
                    $('.all').prop('checked', true);
                }

            });

            // if one checkbox is unchecked remove checked on all menu option

            $(".one").click(function() {
                if ($(this).is(':checked')) {

                    var checked = [];
                    $('.one').each(function() {
                        if ($(this).is(':checked')) {
                            checked.push(1);
                        } else {
                            checked.push(0);
                        }
                    })

                    var flag = 1;

                    if (jQuery.inArray(0, checked) == -1) {
                        flag = 1;

                    } else {
                        flag = 0;
                    }

                    if (flag == 0) {
                        $('.all').prop('checked', false);
                    } else {
                        $('.all').prop('checked', true);
                    }
                } else {

                    $('.all').prop('checked', false);
                }
            });

            // when click all menu  option all checkbox are checked

            $(".all").click(function() {
                if ($(this).is(':checked')) {
                    $('.one').prop('checked', true);
                } else {
                    $('.one').prop('checked', false);
                }
            })
        </script>
        <script>
            $('#download_enable').on('change', function() {
                if ($('#download_enable').is(':checked')) {
                    //show
                    $('#downloadlimit').show();
                } else {
                    //hide
                    $('#downloadlimit').hide();
                }
            });
            $('#free').on('change', function() {
                if ($('#free').is(':checked')) {
                    //show
                    $('#planAmount').hide();
                } else {
                    //hide
                    $('#planAmount').show();
                }
            });
        </script>
    @endsection
