<div class="dropdown">
  <button class="btn btn-round btn-outline-primary" type="button"
      id="CustomdropdownMenuButton1" data-toggle="dropdown"
      aria-haspopup="true" aria-expanded="false"><i
          class="feather icon-more-vertical-"></i></button>
  <div class="dropdown-menu" aria-labelledby="CustomdropdownMenuButton1">
      @can('label.edit')
      <a class="dropdown-item" href="{{ route('category.edit',$id) }}"><i class="feather icon-edit mr-2"></i>{{ __("Editar") }}</a>
      @endcan
      @can('label.delete')
      <a class="dropdown-item btn btn-link" data-toggle="modal" data-target="#delete{{ $id }}">


          <i class="feather icon-delete mr-2"></i>{{ __("Apagar") }}</a>
      </a>
      @endcan
  </div>
</div>

<!-- delete Modal start -->
<div class="modal fade bd-example-modal-sm" id="delete{{$id}}"
  tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleSmallModalLabel">Apagar</h5>
              <button type="button" class="close" data-dismiss="modal"
                  aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <h4>{{ __('Você tem certeza?')}}</h4>
              <p>{{ __('')}}
                  {{ __('Esta ação não poderá ser desfeita.')}}</p>
          </div>
          <div class="modal-footer">
            <form method="POST" action="{{route("category.destroy", $id)}}">
              @method("DELETE")
              @csrf  
                  <button type="reset" class="btn btn-secondary"
                      data-dismiss="modal">Não</button>
                  <button type="submit" class="btn btn-primary">Sim</button>
              </form>
          </div>
      </div>
  </div>
</div>
<!-- delete Model ended -->