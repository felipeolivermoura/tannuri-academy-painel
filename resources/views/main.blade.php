@extends('layouts.theme')
@section('title', __('Welcome'))
@section('main-wrapper')
    <!-- main wrapper -->

    <style>
        .snip1404 .plan-select button {
            background-color: var(--background_black_bg_color);
            border: 3px solid var(--blue_border_color);
            color: var(--text_white_color);
            text-decoration: none;
            padding: 12px 20px;
            font-size: 0.75em;
            font-weight: 600;
            border-radius: 20px;
            text-transform: uppercase;
            letter-spacing: 2px;
            display: inline-block;
        }

        .snip1404 .plan-select button:hover {
            background-color: var(--blue_border_color);
        }
    </style>

    <section id="main-wrapper" class="main-wrapper home-page">

        <div id="home-out-section-slider" class="home-out-section-slider home-out-section owl-carousel">
            @if (isset($blocks) && count($blocks) > 0)
                @foreach ($blocks as $block)
                    <div class="slider-block">
                        <div class="home-out-section-img">
                            <img src="{{ url('images/main-home/' . $block->image) }}" class="img-fluid" alt="">
                            <div
                                class="overlay-bg {{ $block->left == 1 ? 'gredient-overlay-left' : 'gredient-overlay-right' }} ">
                            </div>
                            <div class="home-out-section-dtl">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div
                                            class="col-md-6 col-sm-7 {{ $block->left == 1 ? 'col-md-offset-6 col-sm-offset-6 col-sm-6 col-md-6 text-right' : '' }}">
                                            <h2 class="section-heading">{{ $block->heading }}</h2>
                                            <p class="section-dtl {{ $block->left == 1 ? 'pad-lt-100' : '' }}">
                                                {{ $block->detail }}</p>
                                            @if ($block->button == 1)
                                                @if ($block->button_link == 'login')
                                                    @guest
                                                        <a href="{{ url('login') }}"
                                                            class="btn btn-prime">{{ $block->button_text }}</a>
                                                    @endguest
                                                @else
                                                    @guest
                                                        <a href="{{ url('register') }}"
                                                            class="btn btn-prime">{{ $block->button_text }}</a>
                                                    @endguest
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>

        <!-- GENRES -->
        <div style="margin-top: 25px">
            <div class="container-fluid" id="post-data">
                @if (count($getallgenre) > 0)
                    <div class="owl-carousel owl-theme image-genre-list">
                        @foreach ($getallgenre as $genre)
                            <div class="item">
                                    @if ($auth && getSubscription()->getData()->subscribed == true)
                                        <a href="{{ route('show.in.genre', $genre->id) }}">
                                            <div class="protip text-center" data-pt-placement="outside">
                                                @if ($genre->image != null)
                                                    <img data-src="{{ url('images/genre/' . $genre->image) }}"
                                                        class="img img-responsive genreview owl-lazy">
                                                @else
                                                    <img data-src="{{ url('/images/default-thumbnail.jpg') }}"
                                                        class="img img-responsive genreview owl-lazy">
                                                @endif
                                            </div>
                                             @if (!file_exists(public_path('images/genre/' . $genre->image)))
                                                <div class="content">
                                                    <h1 class="content-heading">{{ $genre->name }}</h1>
                                                </div>
                                            @endif
                                        </a>
                                    @else
                                        <a href="{{ route('show.in.guest.genre', $genre->id) }}">
                                            <div class="protip text-center" data-pt-placement="outside">
                                                @if ($genre->image != null)
                                                    <img data-src="{{ url('images/genre/' . $genre->image) }}"
                                                        class="img img-responsive genreview owl-lazy">
                                                @else
                                                    <img data-src="{{ url('/images/default-thumbnail.jpg') }}"
                                                        class="img img-responsive genreview owl-lazy">
                                                @endif
                                            </div>
                                            @if (!file_exists(public_path('images/genre/' . $genre->image)))
                                                <div class="content">
                                                    <h1 class="content-heading">{{ $genre->name }}</h1>
                                                </div>
                                            @endif
                                        </a>
                                    @endif

                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>

        <!-- Pricing plan main block -->

        @if (isset($remove_subscription) && $remove_subscription == 0)
            @if (isset($plans) && count($plans) > 0)
                <div class="purchase-plan-main-block  main-home-section-plans" style="background-image: url(assets/images/background.png); background-repeat: no-repeat; background-position-x: right; background-size: contain;">
                    <div class="panel-setting-main-block panel-subscribe">
                        <div class="container-fluid">
                            <div class="plan-block-dtl">
                                <h3 class="plan-dtl-heading">{{ __('Take your Jiu-Jitsu to the next level') }}</h3>
                                <h4 class="plan-dtl-heading">{{ __('Join us today with Paypal.') }}</h4>
                            </div>
                            @if (Auth::check())
                                @php
                                    $id = Auth::user()->id;
                                    $getuserplan = App\PaypalSubscription::where('status', '=', '1')
                                        ->where('user_id', $id)
                                        ->orderBy('id', 'DESC')
                                        ->first();
                                    
                                @endphp
                            @endif
                            <?php
                            $today = date('Y-m-d h:i:s');
                            ?>

                            <div class="snip1404 row" id="section-plan">
                                @foreach ($plans as $plan)
                                    @if ($plan->delete_status == 1)
                                        @if ($plan->status != 'inactive')
                                            <div class="col-lg-3 col-sm-6" style="margin-top:15px; margin-left: 7px; margin-right: 7px; background-position: center; background-repeat: no-repeat; background: url(images/plan_background.png); height: 600px; border: 1px solid #c2c0c0; border-radius: 20px;">
                                                <div style="position: absolute; left: 0; border-top-left-radius: 20px; border-top-right-radius: 20px; border-bottom: 1px solid #c2c0c0; margint-top: 1px; height:80px; width: 100%; @if($plan->interval_count == 12) background: linear-gradient(to right, red, #b34404); @else background-color: #4f4e4e; @endif display: flex; justify-content: center; align-items: center; font-size: 18px">
                                                {{ $plan->name }}
                                                </div>
                                                <div style="margin-top: 80px; width: 100%; height: 60px; display: flex; flex-direction: column; justify-content: center; align-items: center;">
                                                    <div style="width: 70%; height: 70px; border-bottom-width: 2px; border-bottom-style: solid; border-image: linear-gradient(to right, red, #b34404) 1; display: flex; flex-direction: column; justify-content: center; align-items: center; padding-top: 15px; padding-bottom: 15px;">
                                                        <img src="images/plan_icon.png" width="65" />
                                                    </div>
                                                </div>
                                                <div style="margin-top: 20px; display: flex; flex-direction: column; justify-content: center; align-items: center; text-align: center;">
                                                    <div style="font-size: 16px; font-weight: 500; color: #c3c7c4">
                                                        @if($plan->free == 1)
                                                            Acess to <br />the Begginers Course.
                                                        @else
                                                            Acess to all content.
                                                        @endif
                                                    </div>
                                                    <div style="padding-left: 20px; padding-right: 20px; margin-top: 10px; font-size: 16px; font-weight: 500; color: #9fa19f">
                                                        @if($plan->free == 1)
                                                            All basic positions, drills and self defences
                                                        @else
                                                            Classes divide by belts and techiques. All positions, drills, self defence and much more.
                                                        @endif
                                                    </div>

                                                    <div style="margin-top: 30px; width: 130px; height: 130px; border-radius: 100px; border: 3px solid #d15400; display: flex; flex-direction: column; justify-content: center; align-items: center; font-size: 18px; background-color:rgba(0, 0, 0, 0.6);">
                                                        @php
                                                            if($plan->interval_count == 12)
                                                                $amount = explode(".", $plan->amount / 12);
                                                            else
                                                                $amount = explode(".", $plan->amount);
                                                        @endphp
                                                        <div style="display: flex;">
                                                            <span style="font-size: 22px; font-weight: 500;">U$</span> <span style="display: inline-block; margin-left: 4px; font-size: 22px; font-weight: 400;">{{ $amount[0] }}</span>,<span style="display: inline-block; margin-top: -1px; font-size: 18px; font-weight: 400;">{{ $amount[1] }}</span>
                                                        </div>
                                                        @if($plan->free != 1)
                                                        <div style="font-size: 14px; font-weight: 600;">
                                                            per month
                                                        </div>
                                                        @endif
                                                    </div>

                                                    <div style="margin-top: 30px; font-size: 16px; font-weight: 600; color: #c3c7c4">
                                                    @if($plan->free == 1)
                                                        32 BJJ tutorial videos
                                                    @else
                                                        380 BJJ tutorial videos
                                                    @endif
                                                    </div>

                                                    @php
                                                        $session_amount = Session::has('coupon_applied') ? Session::get('coupon_applied')['amount'] : 0
                                                    @endphp

                                                    <div class="plan-select">
                                                        @auth
                                                            @if (isset($getuserplan['package_id']) &&
                                                                $getuserplan['package_id'] == $plan->id &&
                                                                $getuserplan->status == '1' &&
                                                                $today <= $getuserplan->subscription_to)
                                                                <a class="btn btn-prime btn-prime-bg">{{ __('Already Subscribe') }}</a>
                                                            @else
                                                                @if($plan->free == 1 && $plan->status == 'status')
                                                                <form action="{{ route('free.package.subscription', $plan->id) }}" method="POST">
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-prime btn-prime-bg">{{ __('Subscribe') }}</button>
                                                                </form>
                                                                @elseif($plan->amount > 0)
                                                                    @if(env('PAYPAL_CLIENT_ID') != NULL && env('PAYPAL_SECRET_ID') != NULL && env('PAYPAL_MODE') != NULL)
                                                                        {!! Form::open(['method' => 'POST', 'action' => 'PaypalController@postPaymentWithpaypal']) !!}
                                                                            <input type="hidden" name="plan_id" value="{{$plan->id}}">
                                                                            <input type="hidden" name="amount" value="{{$plan->amount - $session_amount}}">
                                                                            <button class="btn btn-prime btn-prime-bg"><i class="fa fa-credit-card"></i> {{__('Subscribe via')}} Paypal</button>
                                                                        {!! Form::close() !!}
                                                                    @endif
                                                                @else
                                                                    {!! Form::open([
                                                                        'method' => 'POST',
                                                                        'action' => ['ManualPaymentController@freePackageSubscription', $plan->id],
                                                                        'files' => true,
                                                                    ]) !!}
                                                                    <button type="submit" class="btn btn-prime btn-prime-bg">{{ __('Subscribe') }}</button>
                                                                    {!! Form::close() !!}
                                                                @endif
                                                            @endif
                                                        @else
                                                            <a class="btn btn-prime btn-prime-bg" href="{{ route('register') }}">{{ __('Register Now') }}</a>
                                                        @endauth
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif




        <!-- end featured main block -->
        <!-- end out section -->
    </section>
    <!-- end main wrapper -->
@endsection
@section('script')
    <style>
        .btn-prime-bg{
            background: linear-gradient(to right, red, #b34404);
            background-color: transparent !important;
        }
    </style>
    <script>
        $(".image-genre-list").owlCarousel({
            margin:10,
            loop:true,
            autoplay:true,
            nav:false,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:3.5,
                    merge:true,
                                        
                },
                1000:{
                    items:5
                }
            }
        });

        @if (isset(Auth::user()->multiplescreen))
            @if (Auth::user()->multiplescreen->activescreen != null)
                $(document).ready(function() {

                    $('#showM').hide();

                });
            @else
                $(document).ready(function() {

                    $('#showM').modal();

                });
            @endif
        @endif
    </script>
@endsection
