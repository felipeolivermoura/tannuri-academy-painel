@php
    $disabled = true;
@endphp

<div class="leftbar">
    <!-- Start Sidebar -->
    <div class="sidebar">               
        <!-- Start Navigationbar -->
        <div class="navigationbar">
            <div class="vertical-menu-detail">
                
                <div class="tab-content" id="v-pills-tabContent">

                   <div class="tab-pane fade active show" id="v-pills-dashboard" role="tabpanel" aria-labelledby="v-pills-dashboard">

                    <ul class="vertical-menu">
                        <div class="logobar">
                            <a href="{{ url('/') }}" class="logo logo-large">
                                <img style="object-fit:scale-down;" src="{{url('images/logo/'.$logo)}}"
                                    class="img-fluid" alt="logo">
                            </a>
                        </div>
                      
                        <li class="{{ Nav::isRoute('dashboard') }}">
                            <a class="nav-link" href="{{url('/admin')}}">
                                <i class="feather icon-pie-chart text-secondary"></i>
                                <span>{{__('Dashboard')}}</span>
                            </a>
                        </li>

                        @canany(['users.view','roles.view'])
                            <li class="{{ Nav::isResource('users') }}{{ Nav::isResource('roles') }}">
                                <a href="javaScript:void();" class="menu"><i class="feather icon-users text-secondary"></i>
                                    <span>{{ __('Usuários') }}<div class="sub-menu truncate">{{__('Todos os usuários')}}</div></span><i class="feather icon-chevron-right"></i>
                                </a>
                                <ul class="vertical-submenu">
                                    @can('users.view')
                                    <li>
                                        <a class="{{ Nav::isResource('users') }}"
                                            href="{{url('/admin/users')}}">{{ __('Usuários') }}</a>
                                    </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcanany

                        @if(Module::find('Producer') && Module::find('Producer')->isEnabled())
                            @can(['producer-content.manage','packageforproducer.view','pendingrequest.view'])
                                <li class="{{ Nav::isRoute('addedmovies') }} {{ Nav::isRoute('addedTvSeries') }} {{ Nav::isRoute('addedLiveTv') }}
                                {{ Nav::isResource('packageforproducer') }} {{ Nav::isResource('pendingrequest') }}">
                                    <a href="javaScript:void();" class="menu"><i class="feather icon-users text-secondary"></i>
                                    <span>{{ __('Producer') }}<div class="sub-menu truncate">{{__('Producer Package, All Producer Request, Added Movies, Added Tv Series, Added Live Tv, Payment Details, Payment Settings, Revenue Request')}}</div></span><i class="feather icon-chevron-right"></i>
                                    </a>
                                    <ul class="vertical-submenu">
                                        @can('packageforproducer.view')
                                        <li>
                                            <a class="{{ Nav::isResource('packageforproducer') }}"
                                             href="{{url('/admin/packagesforproducer')}}">{{ __('Producer Package') }}</a>
                                        </li>
                                        @endcan
                                        @if($button->producer_approval==1)
                                        @can('pendingrequest.view')
                                        <li>
                                            <a class="{{ Nav::isResource('pendingrequest') }} {{ Nav::isResource('paymentdetails') }} 
                                            {{ Nav::isResource('producerrevenue') }} "
                                            href="{{url('/admin/pendingrequest')}}">{{ __('All Producer Request') }}</a>
                                        </li>
                                        @endcan
                                        @endif
                                        <li>
                                            <a class="{{ Nav::isRoute('addedmovies') }}" 
                                            href="{{route('addedmovies')}}">{{ __('Added Movies') }}</a>
                                        </li>
                                        <li>
                                            <a class="{{ Nav::isResource('paymentdetails') }}" 
                                            href="{{url('/admin/payment-details')}}">{{ __('Payment Details') }}</a>
                                        </li>
                                        <li>
                                            <a class="{{ Nav::isResource('producerrevenue') }}" 
                                            href="{{url('/admin/producer-settings')}}">{{ __('Payment Settings') }}</a>
                                        </li>
                                        <li>
                                            <a class="{{ Nav::isResource('producerrevenuerequest') }}" 
                                            href="{{url('/admin/producer-revenue-request')}}">{{ __('Revenue Request') }}</a>
                                        </li>
                                    </ul>
                                </li>
                            @endcanany
                        @endif
                        @canany(['menu.view','menu.create','menu.edit','menu.delete','package.view','package-feature.view'])
                            <li class="{{ Nav::isResource('menu') }}">
                                <a href="{{url('/admin/menu')}}">
                                    <i class="ion ion-ios-menu"></i><span>{{ __('Menu') }}</span>
                                </a>
                            </li>
                            <li class="{{ Nav::isResource('packages') }}{{ Nav::isResource('package_feature') }}">
                                <a href="javaScript:void();" class="menu"><i class="ion ion-md-paper"></i>
                                <span>{{ __('Pacotes') }}<div class="sub-menu truncate">{{__('Planos, Recursos dos planos')}}</div></span><i class="feather icon-chevron-right"></i>
                                </a>
                                <ul class="vertical-submenu">
                                    @can('package.view')
                                    <li>
                                         <a class="{{ Nav::isResource('packages') }}"
                                            href="{{url('/admin/packages')}}">{{ __('Planos') }}</a>
                                    </li>
                                    @endcan
                                    @can('package-feature.view')
                                    <li>
                                        <a class="{{ Nav::isResource('package_feature') }}"
                                         href="{{url('/admin/package_feature')}}">{{ __('Recursos dos planos') }}</a>
                                    </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcanany

                        @canany(['movies.view'])
                        @can('movies.view')
                        <li class="{{ Nav::isResource('movies') }}">
                            <a href="{{url('/admin/movies')}}">
                                <i class="fa fa-file-movie-o"></i><span>{{ __('Vídeos') }}</span>
                            </a>
                        </li>
                        @endcan
                        @endcanany
                        @can('genre.view')
                        <li class="{{ Nav::isResource('genres') }}">
                                <a
                                href="{{url('/admin/genres')}}">
                                <i class="fa fa-file-movie-o"></i><span>{{ __('Tipo de faixa') }}</span>
                                </a>
                        </li>
                        @endcan
                        <li>
                            <a class="{{ Nav::isResource('category') }}"
                                href="{{url('/admin/category')}}">
                                <i class="fa fa-file-movie-o"></i><span>{{ __('Categorias') }}</span>
                            </a>
                        </li>
                        <li>
                            <a class="{{ Nav::isResource('sub_category') }}"
                                href="{{url('/admin/sub_category')}}">
                                <i class="fa fa-file-movie-o"></i><span>{{ __('Sub Categorias') }}</span>
                            </a>
                        </li>

                        @canany(['coupon.view','notification.manage','affiliate.settings','affiliate.history','comment-settings.comments','comment-settings.subcomments','fake.views'])
                            <li class="{{ Nav::isResource('coupons') }} {{ Nav::isResource('notification') }} {{ Nav::isRoute('admin.affilate.settings') }} 
                            {{ Nav::isRoute('admin.affilate.dashboard') }} {{ Nav::isRoute('admin.comment.index') }} {{ Nav::isRoute('admin.subcomment.index') }} 
                            {{ Nav::isResource('fakeViews') }}">
                            <a href="javaScript:void();" class="menu"><i class="socicon-itchio"></i>
                                <span>{{ __('Marketing') }}<div class="sub-menu truncate">{{__('Cupons, comentários e outros')}}</div></span><i class="feather icon-chevron-right"></i>
                            </a>
                            <ul class="vertical-submenu">
                                @can('coupon.view')
                                <li>
                                    <a class="{{ Nav::isResource('coupons') }}"
                                        href="{{url('/admin/coupons')}}">{{ __('Cupons') }}</a>
                                </li>
                                @endcan
                                @can('comment-settings.comments')
                                <li>
                                    <a class="{{ Nav::isResource('admin.comment.index') }}"
                                        href="{{url('/admin/comments')}}">{{ __('Comentários') }}</a>
                                </li>
                                @endcan
                                @can('comment-settings.subcomments')
                                <li>
                                    <a class="{{ Nav::isResource('admin.subcomment.index') }}"
                                        href="{{url('/admin/subcomments')}}">{{ __('Respostas de comentários') }}</a>
                                </li>
                                @endcan
                            </ul>
                            @endcanany
                            @canany('reports.revenue','reports.user-subscription')
                                <li class="{{ Nav::isRoute('device_history') }} {{ Nav::hasSegment('report') }} 
                                {{ Nav::isRoute('revenue.report')}} {{ Nav::isRoute('view.track') }}">
                                    <a href="javaScript:void();"><i class="socicon-itchio"></i>
                                        <span>{{ __('Relatórios') }}<div class="sub-menu truncate">{{__('Assinaturas e faturamento')}}</div></span><i class="feather icon-chevron-right"></i>
                                    </a>
                                    <ul class="vertical-submenu">
                                        @if(Auth::user()->is_assistant != 1 && App\PaypalSubscription::count()>0)
                                        @can('reports.user-subscription')
                                        <li>
                                            <a class="{{ Nav::isResource('plan') }}" 
                                            href="{{url('/admin/plan')}}">{{ __('Relatório de assinantes') }}</a>
                                        </li>
                                        @endcan
                                        @endif
                                        @can('reports.revenue')
                                        <li>
                                            <a class="{{ Nav::isRoute('revenue.report')}}"
                                            href="{{url('admin/report_revenue')}}">{{ __('Relatório de receita') }}</a>
                                        </li>
                                        @endcan
                                    </ul>
                                </li>
                        @endcanany

                        @if(!$disabled)
                        @canany(['front-settings.sliders','front-settings.sliders','front-settings.social-icon'])
                            <li class="header">{{ __('SETTING') }}</li>
                            <li class="{{ Nav::isResource('home_slider') }} {{ Nav::isResource('landing-page') }} 
                            {{ Nav::isResource('auth-page-customize') }} {{ Nav::isRoute('social.ico') }} 
                            {{ Nav::isResource('home-block') }} {{ Nav::isResource('custom_page') }} {{ Nav::isResource('faqs') }}">
                                <a href="javaScript:void();"><i class="fa fa-sliders"></i>
                                    <span>{{ __('Site-Customization') }}<div class="sub-menu truncate">{{__('Slider Settings, Landing Page, Custom Pages, Sign In Sign Up, Social Url Setting, Promotion Settings, Faq')}}</div></span><i class="feather icon-chevron-right"></i>
                                </a>
                                <ul class="vertical-submenu">
                                    @can('front-settings.sliders')
                                    <li>
                                        <a class="{{ Nav::isResource('home_slider') }}"
                                            href="{{url('/admin/home_slider')}}">{{ __('Slider Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('front-settings.sliders')
                                    <li>
                                        <a class="{{ Nav::isResource('landing-page') }}"
                                        href="{{url('admin/customize/landing-page')}}">{{ __('Landing Page') }}</a>
                                    </li>
                                    @endcan
                                    @can('front-settings.social-icon')
                                    <li>
                                        <a class="{{ Nav::isResource('social.ico') }}"
                                        href="{{route('social.ico')}}">{{ __('Social Url Setting') }}</a>
                                    </li>
                                    @endcan

                                    @can('front-settings.short-promo')
                                    @if($disabled)
                                    <li>
                                        <a class="{{ Nav::isResource('home-block') }}"
                                            href="{{url('/admin/home-block')}}">{{ __('Promotion Settings') }}</a>
                                    </li>
                                    @endif
                                    @endcan
                                </ul>
                            </li>
                        @endcanany
                        @endif

                        @if(!$disabled)
                        @canany(['site-settings.genral-settings','site-settings.seo','site-settings.api-settings',
                        'site-settings.social-login-settings','site-settings.chat-setting','site-settings.pwa',
                        'site-settings.color-option','manual-payment.view','pushnotification.settings',
                        'site-settings.manualpayment','site-settings.player-setting','ads.view','googleads.view',
                        'site-settings.adsense','site-settings.termsandcondition','site-settings.privacy-policy',
                        'site-settings.refund-policy','site-settings.style-settings','site-settings.copyrights',
                        'site-settings.language'])
                            <li class="{{ Nav::isResource('settings') }} {{ Nav::isResource('seo') }} {{ Nav::isResource('mail') }} {{ Nav::isResource('api') }} {{Nav::isRoute('chat.index')}} 
                            {{ Nav::isRoute('sociallogin') }} {{ Nav::isRoute('term_con') }}{{ Nav::isRoute('manual.payment.gateway') }} 
                            {{ Nav::isRoute('pri_pol') }} {{ Nav::isRoute('refund_pol') }}{{ Nav::isRoute('adsense') }} 
                            {{ Nav::isRoute('copyright') }} {{ Nav::isRoute('term_con') }} {{ Nav::isRoute('pwa.setting.index') }} 
                            {{ Nav::isRoute('player.set') }} {{ Nav::isRoute('ads') }}  {{ Nav::isResource('manual-payment') }} 
                            {{ Nav::hasSegment('blocker') }} {{ Nav::isResource('languages') }} {{ Nav::isRoute('google.ads') }} {{ Nav::isRoute('admin.color.scheme') }}">
                                <a href="javaScript:void();"><i class="ion ion-ios-settings"></i>
                                    <span>{{ __('Site-Setting') }}<div class="sub-menu truncate">{{__('General Settings, Seo Settings, API Settings, Mail Settings, Social Login Settings, Chat Settings, PWA Settings, Color Option, Manual Payment Gateway, Push Notification, Manual Payments, Adsense Settings, Terms Condition, Privacy Policy, Refund Policy, Copyright, Copyright, Languages, Static Words, Currency')}}</div></span><i class="feather icon-chevron-right"></i>
                                </a>
                                <ul class="vertical-submenu">
                                    @can('site-settings.genral-settings')
                                    <li>
                                        <a class="{{ Nav::isRoute('settings') }}" 
                                        href="{{url('admin/settings')}}">{{ __('General Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.seo')
                                    <li>
                                        <a class="{{ Nav::isResource('seo') }}" 
                                        href="{{url('/admin/seo')}}">{{ __('Seo Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.api-settings')
                                    <li>
                                        <a class="{{ Nav::isResource('api') }}" 
                                        href="{{url('admin/api-settings')}}">{{ __('API Settings') }}</a>
                                    </li>
                                    <li>
                                        <a class="{{ Nav::isResource('mail') }}" 
                                        href="{{route('mail.getset')}}">{{ __('Mail Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.social-login-settings')
                                    <li>
                                        <a class="{{ Nav::isRoute('sociallogin') }}" 
                                        href="{{url('/admin/social-login')}}">{{ __('Social Login Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.chat-setting')
                                    <li>
                                        <a class="{{ Nav::isRoute('chat.index') }}" 
                                        href="{{route('chat.index')}}">{{ __('Chat Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.pwa')
                                    <li>
                                        <a class="{{ Nav::isRoute('pwa.setting.index') }}" 
                                        href="{{route('pwa.setting.index')}}">{{ __('PWA Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.color-option')
                                    <li>
                                        <a class="{{ Nav::isRoute('admin.color.scheme') }}" 
                                        href="{{route('admin.color.scheme')}}">{{ __('Color Option') }}</a>
                                    </li>
                                    @endcan
                                    
                                    @can('manual-payment.view')
                                    <li>
                                        <a class="{{ Nav::isRoute('manual.payment.gateway') }}" 
                                        href="{{route('manual.payment.gateway')}}">{{ __('Manual Payment Gateway') }}</a>
                                    </li>
                                    @endcan
                                    @can('pushnotification.settings')
                                    <li>
                                        <a class="{{ Nav::isRoute('admin.push.noti.settings') }}" 
                                        href="{{route('admin.push.noti.settings')}}">{{ __('Push Notification') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.manualpayment')
                                    <li>
                                        <a class="{{ Nav::isResource('manual-payment') }}" 
                                        href="{{url('/admin/manual-payment')}}">{{ __('Manual Payments') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.adsense')
                                    <li>
                                        <a class="{{ Nav::isRoute('adsense') }}" 
                                        href="{{url('/admin/adsensesetting')}}">{{ __('Adsense Settings') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.termsandcondition')
                                    <li>
                                        <a class="{{ Nav::isRoute('term_con') }}" 
                                        href="{{url('admin/term&con')}}">{{ __('Terms Condition') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.privacy-policy')
                                    <li>
                                        <a class="{{ Nav::isRoute('pri_pol') }}" 
                                        href="{{url('admin/pri_pol')}}">{{ __('Privacy Policy') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.refund-policy')
                                    <li>
                                        <a class="{{ Nav::isRoute('refund_pol') }}" 
                                        href="{{url('admin/refund_pol')}}">{{ __('Refund Policy') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.copyrights')
                                    <li>
                                        <a class="{{ Nav::isRoute('copyright') }}" 
                                        href="{{url('admin/copyright')}}">{{ __('Copyright') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.style-settings')
                                    <li>
                                        <a class="{{ Nav::isRoute('customstyle') }}" 
                                        href="{{url('admin/custom-style-settings')}}">{{ __('Custom Style') }}</a>
                                    </li>
                                    @endcan
                                    @can('site-settings.language')
                                    <li>
                                        <a class="{{ Nav::isResource('languages') }}" 
                                        href="{{url('/admin/languages')}}">{{ __('Languages') }}</a>
                                    </li>
                                    @endcan
                                    {{-- @can('site-settings.language')
                                    <li>
                                        <a class="{{ Nav::isResource('StaticWords') }}" 
                                        href="{{url('/admin/custom-static-words')}}">{{ __('Static Words') }}</a>
                                    </li>
                                    @endcan --}}
                                    @can('site-settings.currency')
                                    <li>
                                        <a class="{{ Nav::isResource('currency') }}" 
                                        href="{{route('currency.index')}}">{{ __('Currency') }}</a>
                                    </li>
                                    @endcan
                                </ul>
                            </li>
                        @endcanany
                        @endif

                        @if(!$disabled)
                        @canany(['site-settings.player-settings','ads.view','googlead.view'])
                            <li class="{{ Nav::isRoute('player.set') }} {{ Nav::isRoute('ads') }} {{ Nav::isRoute('google.ads') }}">
                                <a href="javaScript:void();"><i class="socicon-play"></i>
                                    <span>{{ __('Player Setting') }}<div class="sub-menu truncate">{{__('Player Customization, Advertise, Google Advertise')}}</div></span><i class="feather icon-chevron-right"></i>
                                </a>
                                <ul class="vertical-submenu">
                                    @can('site-settings.player-setting')
                                    <li>
                                         <a class="{{ Nav::isRoute('player.set') }}" 
                                         href="{{route('player.set')}}">{{ __('Player Customization') }}</a>
                                    </li>
                                    @endcan
                                    
                                    @can('googleads.view')
                                    <li>
                                         <a class="{{ Nav::isRoute('google.ads') }}" 
                                         href="{{route('google.ads')}}">{{ __('Google Advertise') }}</a>
                                    </li>
                                    @endcan

                                    @if(Module::find('Advertise') && Module::find('Advertise')->isEnabled())
                                    @can('ads.view')
                                    <li>
                                         <a class="{{ Nav::isRoute('ads') }}" 
                                         href="{{url('admin/ads')}}">{{ __('Advertise') }}</a>
                                    </li>
                                    @endcan
                                    @endif

                                </ul>
                            </li>
                        @endcanany
                        @endif
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
        <!-- End Navigationbar -->
    </div>
    <!-- End Sidebar -->
</div>